package com.muevaelvolante.racenet;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.multidex.MultiDexApplication;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.muevaelvolante.racenet.commons.Constants;
import com.muevaelvolante.racenet.model.partner.Partner;
import com.muevaelvolante.racenet.model.post.Post;
import com.muevaelvolante.racenet.model.route.Stopover;
import com.muevaelvolante.racenet.model.team.Team;
import com.muevaelvolante.racenet.model.team.Team_;
import com.muevaelvolante.racenet.push.SNSRegisterTask;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Leo on 24/03/2016.
 */
public class Aplication extends MultiDexApplication {

    public static Typeface hbold;
    public static Typeface hregular;
    public static Typeface hlight;
    public static Typeface hmedium;
    public static Typeface hthin;
    public static Typeface hneue;
    public static Typeface lbold;
    public static Typeface llight;
    public static Typeface lblack;
    public static Typeface lregular;
    public static Context context;
    public static boolean is_teblet;
    public static int screenW;
    private Tracker mTracker;
    public static Post newsList;
    public static List<Team_> teamList;
    public static List<Object> partnerList= new LinkedList<>();
    public static List<Stopover> routeList;

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            //mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context= getApplicationContext();
        new SNSRegisterTask(this,null).execute();
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .showImageOnLoading(R.drawable.generic_image_rectangular)
                .showImageOnFail(R.drawable.generic_image_rectangular)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);
        hbold = Typeface.createFromAsset(getAssets(), "h-bold.ttf");
        hlight = Typeface.createFromAsset(getAssets(), "h-light.otf");
        hregular = Typeface.createFromAsset(getAssets(), "h-regular.ttf");
        hthin = Typeface.createFromAsset(getAssets(), "h-thin.otf");
        hmedium = Typeface.createFromAsset(getAssets(), "h-medium.otf");
        hneue = Typeface.createFromAsset(getAssets(), "h-neue.otf");
        lblack = Typeface.createFromAsset(getAssets(), "l-black.ttf");
        lbold = Typeface.createFromAsset(getAssets(), "l-bold.ttf");
        llight = Typeface.createFromAsset(getAssets(), "l-light.ttf");
        lregular = Typeface.createFromAsset(getAssets(), "l-regular.ttf");
        /**
         * creamos el directorio de cache de los json
         */
        File cache= new File(Constants.jsonCacheRootDir);
        if(!cache.exists())
            cache.mkdirs();
        if(getResources().getBoolean(R.bool.is_tablet)){
            is_teblet=true;
        }
        else
            is_teblet=false;
    }

}
