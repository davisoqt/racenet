package com.muevaelvolante.racenet.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.ReproductorYoutube;
import com.muevaelvolante.racenet.commons.Constants;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.ISODateTimeFormat;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Leo on 24/03/2016.
 */
public class Utils {

    private static int screenWidth=-1;

    public static DateTime convertStringToDate(String dat){
        DateTimeFormatter fmt= DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        return fmt.parseDateTime(dat);
    }
    public static DateTime convertStringToDate2(String dat){
        DateTimeFormatter fmt= DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        return fmt.parseDateTime(dat);
    }

    public static String getDateString(DateTime date1){
        DateTime dt= new DateTime();
        Period period= new Period(date1,new DateTime(), PeriodType.yearMonthDayTime().withYearsRemoved().withMonthsRemoved());
        int days=period.getDays();
        int hours=period.getHours();
        int minutes=period.getMinutes();
        Log.d("fecha",date1.toString("dd:MMMM:yyyy"));
//        if(true){
//            DateTimeFormatter formatterBuilder= new DateTimeFormatterBuilder().appendDayOfMonth(2)
//                    .appendLiteral(" ")
//                    .appendMonthOfYearShortText()
//                    .toFormatter();
//            return date1.toString(formatterBuilder);
//        }
        if(days>0){
            return days+" days ago";
        }
        if(hours>0){
            return hours+" hours ago";
        }
        if(minutes > 0)
            return minutes+" min ago";

        return null;
    }

    public static boolean checkConnectivity(Context context) {
        boolean var1 = true;
        NetworkInfo var2 = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (var2 == null || !var2.isConnected() || !var2.isAvailable()) {
            var1 = false;
        }
        return var1;
    }

    public static void sendMailTo(Context activityContext,String emailTo,String text){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",emailTo, null));
        emailIntent.putExtra(Intent.EXTRA_TEXT,text);
        //Log.d("", sb.toString());

        if(activityContext != null){
            activityContext.startActivity(Intent.createChooser(emailIntent, ""));
        }else{
            Log.e("", "appContext is NULL");
        }
    }

    public static void callTo(Context context,String num){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + num));
        if(intent.resolveActivity(context.getPackageManager())!=null)
            context.startActivity(intent);
    }
    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    public static int getScreenWidth(Activity activity) {

        if (screenWidth != -1)
            return screenWidth;
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();

        int width;
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(size);
            width = size.x;
        } else {
            width = display.getWidth();

        }
        screenWidth = width;

        return width;
    }

    public static void resizeView(View view, int width, int height) {
        ViewGroup.LayoutParams layout = view.getLayoutParams();
        layout.width = width;
        layout.height = height;
        view.setLayoutParams(layout);
    }

    /**
     *Limpia una cadena de texto de etiquetas HTML
     * @param cadena
     * @return
     */

    public static String cleanHTMLTags(String cadena)
    {
        cadena = cadena.replaceAll("<(.*?)\\>"," ");//Removes all items in brackets
        cadena = cadena.replaceAll("<(.*?)\\\n"," ");//Must be undeneath
        cadena = cadena.replaceFirst("(.*?)\\>", " ");//Removes any connected item to the last bracket

        return cadena;
    }


    /**
     * Indica si una cadena vacía. Puede serlo por ser NULL o por ser cadena vacía
     * @param s Cadena a comprobar
     * @return True si es una cadena vacía
     */
    public static boolean isEmptyString(String s)
    {
        return s == null || s.equals("");
    }



    public static String converIconNameToUTF8 (String name){


        if(name.equals("icon-weather-broken-clouds"))
            return "\uf142";
        else if(name.equals("icon-weather-few-clouds"))
            return "\uf143";
        else if(name.equals("icon-weather-mist"))
            return "\uf144";
        else if(name.equals("icon-weather-rain"))
            return "\uf145";
        else if(name.equals("icon-weather-scattered-clouds"))
            return "\uf146";
        else if(name.equals("icon-weather-shower-rain"))
            return "\uf147";
        else if(name.equals("icon-weather-snow"))
            return "\uf148";
        else if(name.equals("icon-weather-sunny"))
            return "\uf149";
        else if(name.equals("icon-weather-thunderstorm"))
            return "\uf14a";
        else
            return " ";
    }


    /**
     * Obtener tel tiempo desde hace
     * @param date
     * @author Mueva al Volante
     * @return
     */
    public static String getTimeSinceMuevaVolante(long date){
        String since = "";

        long interval = Math.abs((date-((new Date()).getTime()))/1000);

        int minutes = (int) (interval/60);
        int hours = minutes/60;
        int days = hours/24;

        if(days>0){
            since = days + "d";
        }
        else if(hours>0){
            since = hours + "h";
            since = since.concat(String.format(" %dm", (interval-(hours*60*60))/60));
        }
        else if(minutes>0){
            since = "0h " + minutes + "m";
        }
        else if(interval>0){
            since = interval + "s";
        }

        return since;
    }
    /**
     * Método que indica si un String es nulo o contiene una cadena vacía.
     *
     * @param s String a comprobar.
     * @return True si el String es nulo o contiene la cadena vacía. False otro caso.
     *
     */
    public static boolean isNullOrEmpty(String s) {

        return ( s == null || s.equals("") );

    }


    /**
     * Método que indica si un String es nulo y lo convierte a vacío.
     *
     * @param s String a comprobar.
     * @return "" si el String es nulo.
     *
     */
    public static String replaceIfNull(String s) {

        if ( s == null || s.equals(null) || s.equals("null"))
            return "";
        else
            return s;

    }



    /**
     * Método para convertir unidades "dp" en píxeles.
     *
     * @param context  Contexto.
     * @param dp  Unidades en "dp".
     *
     * @return Devuelve los píxeles correspondiente a la unidad "dp" introducida,
     * según las medidas del dispositivo.
     */
    public static int dpToPixels(Context context, float dp) {

        return (int) (dp * context.getResources().getDisplayMetrics().density + 0.5f);

    }





    /**
     * Método para obtener el código ISO 639-1 del idioma asociado al dispositivo.
     *
     * @return Devuelve un String indicando el código ISO del idioma.
     *
     */
    public static String getLanguageCodeByPhone () {

		/*
		 * (1) Obtenemos el idioma del dispositivo.
		 */
        String code = Locale.getDefault().getLanguage();

        // Buscamos si tiene algún "sublocale".
        int index = code.indexOf("_");

        if (index != -1) {

            // Caso: Tiene un sublocale.

            // En este caso, se lo cortamos.
            code =  code.substring(0, index);

        }

        return code;

    }





    /**
     * Método para establecer una fecha en formato adecuado para la visualización del usuario.
     *
     * @param dateInMillis  Fecha de creación en milisegundos.
     * @param format Formato de la fecha. Si es null se tomará por defecto: "dd-MM-yyyy"
     * @return La fecha y hora en String con el formato .
     *
     */
    public static String getDateString (long dateInMillis, String format) {

        // Hallamos la fecha del hito.
        Calendar c = Calendar.getInstance();

        if (format == null) {

            format = "dd-MM-yyyy";

        }

        java.text.DateFormat formatter = new SimpleDateFormat(format, Locale.getDefault());
        c.setTimeInMillis(dateInMillis);

        // Devolvemos dicha fecha en un String con el formato requerido.
        return formatter.format(c.getTime());

    }





    /**
     * Método para parsear una fecha en string y convertirla a milisegundos.
     *
     * @param format  Si es null, se usa el formato "dd-MM-yyyy".
     * @return Devuelve la fecha en milisegundos.
     */
    public static long getMillisFromDate (String dateString, String format) {

		/*
		 * Conociendo el formato, parseamos el argumento en String y conseguimos la fecha en milisegundos.
		 */

        java.text.DateFormat formatter;

        if ( format == null ) {

            formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

        } else {

            formatter = new SimpleDateFormat(format, Locale.getDefault());

        }

        try {

            return formatter.parse(dateString).getTime();

        } catch (ParseException e) {

            return 0;

        }

    }





    /**
     * Método para lanzar una URL en el navegador por defecto del dispositivo móvil
     *
     * @param context
     * @param url
     *
     */
    public static void openURL (Context context, String url) {

		/*
		 *Comprobamos que, efectivamente, la URL tiene el formato correcto. Si no es así, le añadimos
		 *lo necesario. Tras ello, con la URL ya bien formada, lanzamos el navegador con un Intent.
		 */

        if ( isNullOrEmpty( url ) ) {

            return;

        }

        if ( ! url.startsWith("http://") && ! url.startsWith("https://")) {

            url = "http://".concat(url);

        }

        context.startActivity(new Intent (Intent.ACTION_VIEW, Uri.parse( url )) );

    }





    /**
     * Método que devuelve las dimensiones de la pantalla de ejecución.
     *
     * @param context  Contexto.
     *
     * @return Devuelve un array de entero con dos valores: alto y ancho de la pantalla, en píxeles.
     *
     */
    public static int[] getScreenDimensions (Context context) {

        // Window Manager.
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        // Dimensiones de la pantalla
        DisplayMetrics displaymetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        // Creamos el array.
        int[] height_width = {height, width};

        // Lo devolvemos.
        return height_width;

    }





    /**
     * Método para formar un HTML a partir de una ID de YouTube. Dicho HTML se utiliaza para captar
     * un iframe embebido.
     *
     * @param youtubeID
     * @return
     */
    public static String getHTMLYouTube(String youtubeID) {
        String html = "<!DOCTYPE HTML><html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:og=\"http://opengraphprotocol.org/schema/\" xmlns:fb=\"http://www.facebook.com/2008/fbml\"><head></head> <body style=\"margin:0 0 0 0; padding:0 0 0 0;\"><iframe class=\"youtube-player\" style=\"border: 0; background-color:black; width: 100%; height: 100%; padding:0px; margin:0px\" id=\"ytplayer\" type=\"text/html\" src=\"http://www.youtube.com/embed/"
                + youtubeID + "?fs=0\" frameborder=\"0\">\n" + "</iframe></body></html>";
        return html;
    }





    /**
     * Método para encontrar la ID de un vídeo de YouTube a partir de su URL.
     *
     * @param url
     * @return Devuelve la ID, en formato String, ó null si no se ha podido encontrar.
     */
    public static String getYouTubeID (String url) {

        String pattern = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|watch\\?v%3D|%2Fvideos%2F|embed%‌​2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\n]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(url);

        if( matcher.find() ){

            String id = matcher.group();

            if ( id.length() == 11 ) {

                return id;

            } else {

                return null;

            }

        } else {

            return null;
        }

    }





    /**
     * Método para proceder a la reproducción de un vídeo, de forma nativa, a partir de la URL al mismo.
     *
     * @param context
     * @param url
     */
    public static void playVideo ( Context context, String url ) {

        if ( isNullOrEmpty( url ) ) {

            return;

        }

        try {

            Uri videoUri = Uri.parse( url );
            Intent videoIntent = new Intent(Intent.ACTION_VIEW, videoUri);
            videoIntent.setDataAndType(videoUri, "video/*");
            context.startActivity( videoIntent );

        } catch (Exception e) { }

    }





    /**
     * Método para la reproducción de un vídeo mediante la API de YouTube.
     *
     * @param c
     * @param urlPlayable
     */
    public static void playVideoYoutube ( Context c, String urlPlayable ) {

        try {

            Intent playReportIntent = new Intent(c, ReproductorYoutube.class);
            playReportIntent.setType(MimeTypeMap.getFileExtensionFromUrl(urlPlayable));
            playReportIntent.setData(Uri.parse( urlPlayable ));
            c.startActivity(playReportIntent);

        } catch (Exception e) {

            Toast.makeText(c, R.string.error_generic, Toast.LENGTH_LONG).show();

        }

    }





    /**
     * Método para proceder enviar un e-mail con los parámetros pasados. Se abrirá un "chooser" para
     * poder elegir una de las aplicaciones disponibles en el terminar para enviar el e-mail.
     *
     * @param context
     * @param title  Puede pasarse null.
     * @param description  Puede pasarse null.
     * @param uriToImage  Puede pasarse null.
     */
    public static void shareViaEmail (Context context, String title, CharSequence description,
                                      String uriToImage) {

        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        if ( title != null )
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, title);

        if ( description != null )
            emailIntent.putExtra(Intent.EXTRA_TEXT, description);

        if ( uriToImage != null ) {

            emailIntent.putExtra(Intent.EXTRA_STREAM, uriToImage);
            emailIntent.setType("image/*");

        } else {

            emailIntent.setType("text/plain");

        }
//
//        context.startActivity(Intent.createChooser(emailIntent,
//                context.getResources().getString(R.string.shareviaemail)));

    }





    /**
     * Método que almacena un Bitmap en un archivo local y devuelve el objeto File asociado.
     *
     * @param c
     * @param bitmap
     *
     * @return Devuelve el objeto File asociado al Bitmap recién almacenado.
     */
//    public static File saveBitmap (Context c, Bitmap bitmap) {
//
//        File imagePath = new File(PersistenceFile.getPathDataForApp(c) + "/tempImage.jpeg");
//
//        FileOutputStream fos;
//
//        try {
//
//            fos = new FileOutputStream(imagePath);
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
//            fos.flush();
//            fos.close();
//
//            return imagePath;
//
//        } catch (FileNotFoundException e) {
//
//            return null;
//
//        } catch (IOException e) {
//
//            return null;
//        }
//    }





    /**
     * Método que abre Twitter, ya se la aplicación nativa ó la web, y lleva a la pantalla de
     * publicación de un tuit con el texto de argumento ya establecido en el campo de edición.
     *
     * @param c
     * @param text
     */
    public static void postOnTwitter (Context c, String text) {

        if ( isNullOrEmpty( text ) ) {

            text = "";

        } else {

            text = text.concat(" ");

        }

        try {

            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("twitter://post?message=" + Uri.encode( text )));
            c.startActivity(i);

        } catch (android.content.ActivityNotFoundException e) {

            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("http://twitter.com/intent/tweet?text=" + Uri.encode( text )));
            c.startActivity(i);

        }

    }





    /**
     * Método para lanzar Google Play de manera que el usuario pueda proceder a una actualización
     * de la aplicación.
     *
     * @param c
     */
    public static void goToUpdateGooglePlay (Context c) {

        try {

            c.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.Direct_Market_app
                    .concat(c.getPackageName()))));

        } catch (android.content.ActivityNotFoundException anfe) {

            c.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.URL_Market_app
                    .concat( c.getPackageName() ))));
        }

    }


}
