package com.muevaelvolante.racenet.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.HomeActivity;
import com.muevaelvolante.racenet.model.agenda.Home;
import com.muevaelvolante.racenet.model.post.Category;
import com.muevaelvolante.racenet.model.post.Subcategory;
import com.muevaelvolante.racenet.service.ServiceGenerator;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Leo on 18/08/2016.
 */
public class HomeSearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private boolean category;
    private boolean sub_category;
    List<Category> categoryList;
    List<String> original=new ArrayList<>();
    List<String> data= new ArrayList<>();
    Context context;
    private int plus;
    private Category lastCat;

    public HomeSearchAdapter(List<Category> categoryList, List<String> dat, Context context) {
        this.categoryList = categoryList;
        this.context = context;
        category=true;
        data.add("ALL ARTICLES");
        for (Category c :
                categoryList) {
            data.add(c.getName());
        }
        original=data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case 0:
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_who_is_who_filter_header, parent, false);
                return new HeaderHolder(view);
            case 1:
                View vieww = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_who_is_who_filter_item, parent, false);
                return new Holder(vieww);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position){
            case 0:
                if (category)
                    return 1;
                return 0;
            default:
                return 1;

        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                if (category) {
                    ((Holder) holder).partner.setText(data.get(position - plus));
                } else {
                    ((HeaderHolder) holder).back.setVisibility(View.VISIBLE);
                    ((HeaderHolder) holder).text.setText("Back");
                }
                break;
            case 1:
                ((Holder) holder).partner.setText(data.get(position - plus));
        }
    }

    @Override
    public int getItemCount() {
       if(category){
           plus=0;
           return data.size();
       }
        else{
           plus=1;
           return data.size()+1;
       }
    }

    public class HeaderHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.imageView11)
        View back;
        @Bind(R.id.partner_text)
        TextView text;

        public HeaderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(back.getVisibility()== View.VISIBLE){
                        category=true;
                        data=original;
                        notifyDataSetChanged();
                    }
                    else {

                    }
                }
            });
        }
    }

    public class Holder extends RecyclerView.ViewHolder{

        @Bind(R.id.partner_text)
        TextView partner;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            partner.setTypeface(Aplication.hlight);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(category){
                        if(getAdapterPosition()==0){
                            ((HomeActivity)context).loadNoFilterData();
                            return;
                        }
                        lastCat= categoryList.get(getAdapterPosition()-plus);
                        if(lastCat.getSubcategories()!= null && lastCat.getSubcategories().size()>0){
                            List<String> temp= new ArrayList<String>();
                            for (Subcategory sub :lastCat.getSubcategories()) {
                                temp.add(sub.getName());
                            }data=temp;
                            category=false;
                            notifyDataSetChanged();
                        }else{
                            //lamar a la api
                            ((HomeActivity)context).search(ServiceGenerator.BASE_API_SEARCH_CATEGORY_URL+categoryList.get(getAdapterPosition()-plus).getId());
//                            ((HomeActivity)context).search("http://192.168.101.1:8081/race/search");
                        }

                    }else if(!category){
                        if(getAdapterPosition()==0){

                        }else {
                            ((HomeActivity)context).search(ServiceGenerator.BASE_API_SEARCH_CATEGORY_URL+lastCat.getSubcategories().get(getAdapterPosition()-1).getId());
                        }
                    }
                }
            });
        }
    }
}
