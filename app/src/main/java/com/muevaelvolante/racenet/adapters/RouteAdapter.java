package com.muevaelvolante.racenet.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.internal.LinkedTreeMap;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.RouteDetailActivity;
import com.muevaelvolante.racenet.model.route.Leg;
import com.muevaelvolante.racenet.model.route.Route;
import com.muevaelvolante.racenet.model.route.Stopover;
import com.muevaelvolante.racenet.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Leo on 09/08/2016.
 */
public class RouteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    String selected_tabColor="#454553";
    String unSelected_tabColor="#64454553";
    //last viewHolderTab
    int last_selected; //0 stop_over 1 news
    private boolean stop_over;
    int alto_para_imagenes=0;

    Route route;
    Context context;

    public RouteAdapter(Route route, Context context) {
        this.route = route;
        this.context = context;
        stop_over=true;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case 0:
                return new TabViewHolder(((LayoutInflater) Aplication.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.home_item_tab_item, null));
            case 1:
                return new RouteHolder(((LayoutInflater) Aplication.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.fragment_route_item, null));
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position){
            case 0:
                return 0;
            default:return 1;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        switch (holder.getItemViewType()){
            case 0:
                TabViewHolder tabHolder=  (TabViewHolder)holder;
                ((TabViewHolder) holder).tab_text1.setText("Stopovers");
                ((TabViewHolder) holder).tab_text2.setText("Legs");
                if(stop_over) {
                    last_selected = 0;
                    tabHolder.indicator2.setVisibility(View.INVISIBLE);
                    tabHolder.tab_text2.setTextColor(Color.parseColor(unSelected_tabColor));
                    tabHolder.tab_text1.setTextColor(Color.parseColor(selected_tabColor));
                    if (route.getStopovers() == null || route.getStopovers() .size() == 0)
                        tabHolder.root_indicator2.setVisibility(View.GONE);
                }else{
                    last_selected=1;
                    tabHolder.indicator1.setVisibility(View.INVISIBLE);
                    tabHolder.tab_text1.setTextColor(Color.parseColor(unSelected_tabColor));
                    tabHolder.tab_text2.setTextColor(Color.parseColor(selected_tabColor));
                    if(route.getLegs()== null || route.getLegs().size()==0)
                        tabHolder.root_indicator1.setVisibility(View.GONE);
                }
                break;
            case 1:
                String date;
                String url;
                String title;
               RouteHolder routeHolder =(RouteHolder)holder;
                if(stop_over){
                    last_selected=0;
                    Stopover conferenceNews= route.getStopovers().get(position-1);
                    date=conferenceNews.getDates();
                    title=conferenceNews.getName();
                    if(Aplication.is_teblet){
                        url=((LinkedTreeMap<String,String>)conferenceNews.getMedia().get(0)).get("hi");
                    }
                    else{
                        url=((LinkedTreeMap<String,String>)conferenceNews.getMedia().get(0)).get("med");
                    }
                }else{
                    last_selected=1;
                    Stopover conferenceNews= route.getLegs().get(position-1);
                    date=conferenceNews.getDates();
                    title=conferenceNews.getName();
                    if(Aplication.is_teblet){
                        url=((LinkedTreeMap<String,String>)conferenceNews.getMedia().get(0)).get("hi");
                    }
                    else{
                        url=((LinkedTreeMap<String,String>)conferenceNews.getMedia().get(0)).get("med");
                    }
                }
                routeHolder.date.setText(date);
                routeHolder.title.setText(title);
                if(url==null)
                    routeHolder.image.setVisibility(View.GONE);
                else
                    ImageLoader.getInstance().displayImage(url, routeHolder.image, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {

                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {

                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                            int alto= (int) (Aplication.screenW/proportion);
                            if(alto_para_imagenes==0)
                                alto_para_imagenes=alto;
                            RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams)view.getLayoutParams();
                            ((ImageView)view).setScaleType(ImageView.ScaleType.FIT_XY);
                            lp.height=alto_para_imagenes;
                            lp.width=Utils.getScreenWidth((Activity) context);
                            Log.d(position+"",bitmap.getWidth()+"x"+bitmap.getHeight()+"-"+lp.height);
                            view.setLayoutParams(lp);
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {

                        }
                    });
                break;
        }
    }

    @Override
    public int getItemCount() {
      if(stop_over)
          return route.getStopovers().size()+1;
        else
          return route.getLegs().size()+1;
    }

    class TabViewHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.imageView5)
        View indicator1;
        @Bind(R.id.imageView6)
        View indicator2;
        @Bind(R.id.textView10)
        TextView tab_text1;
        @Bind(R.id.textView9)
        TextView tab_text2;
        @Bind(R.id.root_indicator1)
        View root_indicator1;
        @Bind(R.id.root_indicator2)
        View root_indicator2;

        public TabViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            tab_text1.setTypeface(Aplication.hregular);
            tab_text2.setTypeface(Aplication.hregular);
            LinearLayout.LayoutParams lp= new LinearLayout.LayoutParams(Utils.getScreenWidth((Activity)context), (int) context.getResources().getDimension(R.dimen.tap_height));
            itemView.setLayoutParams(lp);
            root_indicator1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(last_selected==0)
                        return;
                    last_selected=0;
                    stop_over =true;
                    indicator2.setVisibility(View.INVISIBLE);
                    indicator1.setVisibility(View.VISIBLE);
                    tab_text2.setTextColor(Color.parseColor(unSelected_tabColor));
                    tab_text1.setTextColor(Color.parseColor(selected_tabColor));
                    notifyItemRangeRemoved(1,route.getStopovers().size()+1);
                }
            });
            root_indicator2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(last_selected==1)
                        return;
                    last_selected=1;
                    stop_over =false;
                    indicator2.setVisibility(View.VISIBLE);
                    indicator1.setVisibility(View.INVISIBLE);
                    tab_text1.setTextColor(Color.parseColor(unSelected_tabColor));
                    tab_text2.setTextColor(Color.parseColor(selected_tabColor));
                    notifyItemRangeRemoved(1,route.getLegs().size()+1);
                }
            });
        }
    }

    class RouteHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.imageView8)
        ImageView image;
        @Bind(R.id.textView11)
        TextView title;
        @Bind(R.id.textView12)
        TextView date;

        public RouteHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            Bitmap b= BitmapFactory.decodeResource(context.getResources(),R.drawable.generic_image_rectangular);
            float proportion = (float) b.getWidth() / b.getHeight();
            int alto = (int) ((Aplication.screenW) / proportion);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
            lp.height = alto;
            lp.width = Aplication.screenW;
            image.setLayoutParams(lp);
            title.setTypeface(Aplication.hlight);
            date.setTypeface(Aplication.hlight);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent detail= new Intent(context, RouteDetailActivity.class);
                    Bundle data= new Bundle();
                    if(last_selected==0){
                    data.putSerializable("d",route.getStopovers().get(getAdapterPosition()-1));
                    Aplication.routeList=route.getStopovers();}
                    else {data.putSerializable("d",route.getLegs().get(getAdapterPosition()-1));
                    Aplication.routeList=route.getLegs();}
                    data.putInt("pos",getAdapterPosition()-1);
                    detail.putExtras(data);
                    context.startActivity(detail);
                }
            });
        }
    }
}
