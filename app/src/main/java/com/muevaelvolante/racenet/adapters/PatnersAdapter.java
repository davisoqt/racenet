package com.muevaelvolante.racenet.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.fragment.PartnerFragment;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Leo on 09/08/2016.
 */
public class PatnersAdapter extends RecyclerView.Adapter<PatnersAdapter.ViewHolder> {

    private final PartnerFragment fragment;
    List<String> partners;

    public PatnersAdapter(List<String> partners, PartnerFragment fragment) {
        this.partners = partners;
        this.fragment= fragment;
    }

    @Override
    public PatnersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_partner_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PatnersAdapter.ViewHolder holder, int position) {
        holder.partner.setText(partners.get(position));
    }

    @Override
    public int getItemCount() {
        return partners.size();
    }

    class ViewHolder extends  RecyclerView.ViewHolder{

        @Bind(R.id.partner_text)
        TextView partner;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            partner.setTypeface(Aplication.hlight);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fragment.setPartnerSelected(partners.get(getAdapterPosition()));
                    fragment.getItem_adapter().partners=fragment.getPartnerForKey(partners.get(getAdapterPosition()));
                    fragment.getItem_adapter().notifyDataSetChanged();
                    fragment.closePartnersList();
                }
            });
        }
    }
}
