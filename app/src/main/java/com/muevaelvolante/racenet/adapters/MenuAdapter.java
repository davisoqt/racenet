package com.muevaelvolante.racenet.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.fragment.AgendaFragment;
import com.muevaelvolante.racenet.fragment.FragmentMenu;
import com.muevaelvolante.racenet.fragment.NotificationFragment;
import com.muevaelvolante.racenet.fragment.PartnerFragment;
import com.muevaelvolante.racenet.fragment.RaceScheduleFragment;
import com.muevaelvolante.racenet.fragment.RuoteFragment;
import com.muevaelvolante.racenet.fragment.TeamFragment;
import com.muevaelvolante.racenet.fragment.WhoIsWhoFragment;
import com.muevaelvolante.racenet.fragment.ontactFragment;
import com.muevaelvolante.racenet.model.setting.Menu;
import com.muevaelvolante.racenet.model.team.Team;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;


import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {

    Context context;
    FragmentMenu.MenuListener listener;
    ArrayList<Menu> menuItems;
    public void setMenuItems(ArrayList<Menu> menuItems) {
        this.menuItems = menuItems;
    }
    public MenuAdapter(Context context, ArrayList<Menu> menuItems, FragmentMenu.MenuListener listener) {
        this.context = context;
        this.menuItems = menuItems;
        this.listener=listener;
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .showImageOnLoading(null)
                .showImageOnFail(null)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(Aplication.context)
                //.memoryCacheExtraOptions(480,480)
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().destroy();
        ImageLoader.getInstance().init(config);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menu_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.content.setText(menuItems.get(position).getText());
        holder.content.setTypeface(Aplication.hthin);
        Log.d("", "icon: "+menuItems.get(position).getIcon());
        ImageLoader.getInstance().displayImage(menuItems.get(position).getIcon(),holder.image);
    }

    @Override
    public int getItemCount() {
        return menuItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @Bind(R.id.imageView4)
        ImageView image;
        @Bind(R.id.content)
        TextView content;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            //aqui creamos el fragment en dependencia de la opcion de menu
            //Toast.makeText(context,"GOTO "+ menuItems.get(getAdapterPosition()).getText(), Toast.LENGTH_SHORT).show();
            if(menuItems.get(getAdapterPosition()).getID().toLowerCase().equals("teams")){
                listener.gotoSection(TeamFragment.newInstance(menuItems.get(getAdapterPosition()).getURL(), "Teams"));
                return;
            }
            if(menuItems.get(getAdapterPosition()).getID().toLowerCase().equals("route")){
                listener.gotoSection(RuoteFragment.newInstance(menuItems.get(getAdapterPosition()).getURL(),"Route"));
                return;
            }
            if(menuItems.get(getAdapterPosition()).getID().toLowerCase().equals("partners")){
                listener.gotoSection(PartnerFragment.newInstance(menuItems.get(getAdapterPosition()).getURL(),"Partners"));
                return;
            }
            if(menuItems.get(getAdapterPosition()).getID().toLowerCase().equals("whoiswho")){
                listener.gotoSection(WhoIsWhoFragment.newInstance(menuItems.get(getAdapterPosition()).getURL(),"Who is Who"));
                return;
            }
            if(menuItems.get(getAdapterPosition()).getID().toLowerCase().equals("conference-agenda")){
                listener.gotoSection(AgendaFragment.newInstance(menuItems.get(getAdapterPosition()).getURL(),"Conference Agenda"));
                return;
            }
            if(menuItems.get(getAdapterPosition()).getID().equals("SCHEDULE")){
                listener.gotoSection(RaceScheduleFragment.newInstance(menuItems.get(getAdapterPosition()).getURL(),"Race Schedule"));
                return;
            }
//            if(menuItems.get(getAdapterPosition()).getID().toLowerCase().equals("notifications")){
//                listener.gotoSection(NotificationFragment.newInstance(menuItems.get(getAdapterPosition()).getURL(),"Notifications"));
//                return;
//            }
            if(menuItems.get(getAdapterPosition()).getID().toLowerCase().equals("logout")){
                listener.logOut();
                return;
            }
            if(menuItems.get(getAdapterPosition()).getID().toLowerCase().equals("home")){
                listener.gotoSection(null);
                return;
            }
            if(menuItems.get(getAdapterPosition()).getID().toLowerCase().equals("contact")){
                listener.gotoSection(ontactFragment.newInstance(menuItems.get(getAdapterPosition()).getURL(),"Contact"));
                return;
            }
        }
    }
}
