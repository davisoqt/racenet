package com.muevaelvolante.racenet.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.SpeakerDetailActivity;
import com.muevaelvolante.racenet.activity.WhoIsWhoDetailActivity;
import com.muevaelvolante.racenet.model.agenda.Speaker;
import com.muevaelvolante.racenet.model.who.Person;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Leo on 21/08/2016.
 */
public class SpeakersAdapter extends RecyclerView.Adapter<SpeakersAdapter.ViewHolder> {
    ArrayList<Speaker> data;
    ArrayList<Person> filtered;
    Context context;

    public SpeakersAdapter(ArrayList<Speaker> data, Context context) {
        this.data = data;
        this.context = context;
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .showImageOnLoading(R.drawable.generic_image_square)
                .showImageOnFail(R.drawable.generic_image_square)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(Aplication.context)
                //.memoryCacheExtraOptions(480,480)
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().destroy();
        ImageLoader.getInstance().init(config);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.who_is_who_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        String url;
        Speaker data = SpeakersAdapter.this.data.get(position);
        holder.name.setText(data.getFullname());
        holder.role.setText(data.getRole());
        if (Aplication.is_teblet) {
            url = (data).getMedia().get(0).getHi();
        } else {
            url = data.getMedia().get(0).getMed();
        }
        ImageLoader.getInstance().displayImage(url, holder.image, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {

            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                Log.d("", "onLoadingComplete: ");
                if (bitmap != null) {
                    float proportion = (float) bitmap.getWidth() / bitmap.getHeight();
                    int alto = (int) ((Aplication.screenW / 2) / proportion);
                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    lp.height = alto;
                    lp.width = Aplication.screenW / 2;
                    view.setLayoutParams(lp);
                    holder.shadow.setLayoutParams(lp);
                    //((RelativeLayout)view.getParent()).setLayoutParams(lp);
                }
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.imageView9)
        ImageView image;
        @Bind(R.id.textView13)
        TextView name;
        @Bind(R.id.textView14)
        TextView role;
        @Bind(R.id.imageView16)
        View shadow;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            name.setTypeface(Aplication.hlight);
            role.setTypeface(Aplication.hregular);
            Bitmap b= BitmapFactory.decodeResource(context.getResources(),R.drawable.generic_image_square2);
            float proportion = (float) b.getWidth() / b.getHeight();
            int alto = (int) ((Aplication.screenW / 2) / proportion);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
            lp.height = alto;
            lp.width = Aplication.screenW / 2;
            image.setLayoutParams(lp);
            shadow.setLayoutParams(lp);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent detail= new Intent(context, SpeakerDetailActivity.class);
                    Bundle bundle= new Bundle();
                    bundle.putSerializable("p",data.get(getAdapterPosition()));
                    detail.putExtras(bundle);
                    context.startActivity(detail);
                }
            });
        }
    }
}
