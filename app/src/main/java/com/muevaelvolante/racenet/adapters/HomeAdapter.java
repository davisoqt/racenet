package com.muevaelvolante.racenet.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.vision.text.Line;
import com.google.gson.internal.LinkedTreeMap;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.HomeActivity;
import com.muevaelvolante.racenet.activity.NewsDetailActivity;
import com.muevaelvolante.racenet.activity.NewsHighLightDetailActivity;
import com.muevaelvolante.racenet.activity.ReaderActivity;
import com.muevaelvolante.racenet.fragment.AgendaFragment;
import com.muevaelvolante.racenet.fragment.PartnerFragment;
import com.muevaelvolante.racenet.fragment.RaceScheduleFragment;
import com.muevaelvolante.racenet.fragment.RuoteFragment;
import com.muevaelvolante.racenet.fragment.TeamFragment;
import com.muevaelvolante.racenet.fragment.WhoIsWhoFragment;
import com.muevaelvolante.racenet.fragment.ontactFragment;
import com.muevaelvolante.racenet.model.post.ConferenceNews;
import com.muevaelvolante.racenet.model.post.Highlight;
import com.muevaelvolante.racenet.model.post.Post;
import com.muevaelvolante.racenet.model.post.RacenetNews;
import com.muevaelvolante.racenet.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Leo on 04/08/2016.
 */
public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private int plus;

    public void setConference(boolean conference) {
        this.conference = conference;
    }

    boolean conference;//seccion por default
    Post originalPost;
    Post posts;
    String selected_tabColor="#454553";
    String unSelected_tabColor="#64454553";
    //last viewHolderTab
    int last_selected; //0 conference 1 news

    public HomeAdapter(Post posts, Context context,boolean conference) {
        this.posts = posts;
        this.context = context;
        this.conference=conference;
        originalPost=posts;

    }

    Context context;

    public HomeAdapter(Context context) {
        this.context = context;
    }

    public void setReaceNew(ArrayList<RacenetNews> racenetNewses){
        posts.setRacenetNews(racenetNewses);
    }

    public void setConferenceNews(ArrayList<ConferenceNews> conferenceNews){
        posts.setConferenceNews(conferenceNews);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       switch (viewType){
           case 0:
               return new TopViewHolder(((LayoutInflater) Aplication.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.home_item_top, null));
           case 1:
               return new TabViewHolder(((LayoutInflater) Aplication.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.home_item_tab_item, null));
           case 2:
               return new NewsViewHolder(((LayoutInflater) Aplication.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.home_news_item, null));
       }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()){
            case 0:
                ((TopViewHolder)holder).pager.setAdapter(new TopViewPagerAdapter( ((TopViewHolder)holder).pager,((TopViewHolder)holder).root));
                ((TopViewHolder)holder).indicator.setViewPager(((TopViewHolder)holder).pager);
                break;
            case 1:
                TabViewHolder tabHolder=  (TabViewHolder)holder;
                if(conference) {
                    last_selected = 0;
                    tabHolder.indicator2.setVisibility(View.INVISIBLE);
                    tabHolder.tab_text2.setTextColor(Color.parseColor(unSelected_tabColor));
                    tabHolder.tab_text1.setTextColor(Color.parseColor(selected_tabColor));
                    if (posts.getRacenetNews() == null || posts.getRacenetNews().size() == 0)
                        tabHolder.root_indicator2.setVisibility(View.GONE);
                }else{
                    last_selected=1;
                    tabHolder.indicator1.setVisibility(View.INVISIBLE);
                    tabHolder.tab_text1.setTextColor(Color.parseColor(unSelected_tabColor));
                    tabHolder.tab_text2.setTextColor(Color.parseColor(selected_tabColor));
                    if(posts.getConferenceNews()== null || posts.getConferenceNews().size()==0)
                        tabHolder.root_indicator1.setVisibility(View.GONE);
                }
                break;
            case 2:
                String date;
                String url;
                String title;
                String text;
                NewsViewHolder newsViewHolder=(NewsViewHolder)holder;
                List<Object> media;
               if(conference){
                   ConferenceNews conferenceNews= posts.getConferenceNews().get(position-plus);
                   media= conferenceNews.getMedia();
                   date=Utils.getDateString(Utils.convertStringToDate(conferenceNews.getDate()));
                   title=conferenceNews.getTitle();
                   text= conferenceNews.getShortText();
                   if(Aplication.is_teblet){
                       url=((LinkedTreeMap<String,String>)media.get(0)).get("hi");
                   }
                   else{
                       url=((LinkedTreeMap<String,String>)media.get(0)).get("med");
                   }

               }else{
                   RacenetNews conferenceNews= posts.getRacenetNews().get(position-plus);
                   media=conferenceNews.getMedia();
                   date=Utils.getDateString(Utils.convertStringToDate(conferenceNews.getDate()));
                   title=conferenceNews.getTitle();
                   text= conferenceNews.getShortText();
                   if(Aplication.is_teblet){
                       url=((LinkedTreeMap<String,String>)media.get(0)).get("hi");
                   }
                   else{
                       url=((LinkedTreeMap<String,String>)media.get(0)).get("med");
                   }
               }
                newsViewHolder.date.setText(date);
                newsViewHolder.title.setText(title);
                newsViewHolder.short_text.setText(text);
                Log.d("", "image URL: "+ url);
                if(url==null)
                    newsViewHolder.image.setVisibility(View.GONE);
                else
                    ImageLoader.getInstance().displayImage(url, newsViewHolder.image, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {

                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {

                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                            int alto= (int) ((Utils.getScreenWidth((Activity) context)-context.getResources().getDimension(R.dimen.image_margin))/proportion);
                            LinearLayout.LayoutParams lp= (LinearLayout.LayoutParams)view.getLayoutParams();
                            lp.height=alto;
                            lp.width=Utils.getScreenWidth((Activity) context);
                            view.setLayoutParams(lp);
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {

                        }
                    });

        }
    }

    @Override
    public int getItemViewType(int position) {
        switch (position){
            case 0:
                if(plus==2)
                return 0;
                return 1;
            case 1:
                if(plus==2)
                return 1;
                return 2;
            default:return 2;
        }
    }

    @Override
    public int getItemCount() {
        //si no hay hilight, el listado solo tendra 1 elemento mas que el tab
        if(posts.getHighlights()==null || posts.getHighlights().size()>0)
            plus=2;
        else
            plus=1;
        if(posts.getConferenceNews()==null || posts.getConferenceNews().size()<=0)
            this.conference=false;
        if(conference)
        return posts.getConferenceNews().size()+plus;
        else
            return posts.getRacenetNews().size()+plus;
    }

    //Holders
    class TopViewHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.pager)
        ViewPager pager;
        @Bind(R.id.indicator)
        CirclePageIndicator indicator;
        RelativeLayout root;


        public TopViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            root= (RelativeLayout) itemView;
        }
    }

    class TabViewHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.imageView5)
        View indicator1;
        @Bind(R.id.imageView6)
        View indicator2;
        @Bind(R.id.textView10)
        TextView tab_text1;
        @Bind(R.id.textView9)
        TextView tab_text2;
        @Bind(R.id.root_indicator1)
        View root_indicator1;
        @Bind(R.id.root_indicator2)
        View root_indicator2;
        @Bind(R.id.margin)
        View margin;
        @Bind(R.id.tab)
        View tab;
        public TabViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            if(plus==1)
           //     margin.setVisibility(View.INVISIBLE);
            if(posts.getConferenceNews()==null || posts.getConferenceNews().size()<=0){
                root_indicator1.setVisibility(View.GONE);
            }
            if(posts.getRacenetNews()==null || posts.getRacenetNews().size()<=0){
                root_indicator2.setVisibility(View.GONE);
            }
            tab_text1.setTypeface(Aplication.hregular);tab_text2.setTypeface(Aplication.hregular);
            LinearLayout.LayoutParams lp= new LinearLayout.LayoutParams(Utils.getScreenWidth((Activity)context), (int) context.getResources().getDimension(R.dimen.tap_height));
            tab.setLayoutParams(lp);
            root_indicator1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(last_selected==0)
                        return;
                    last_selected=0;
                    conference=true;
                    indicator2.setVisibility(View.INVISIBLE);
                    indicator1.setVisibility(View.VISIBLE);
                    tab_text2.setTextColor(Color.parseColor(unSelected_tabColor));
                    tab_text1.setTextColor(Color.parseColor(selected_tabColor));
                    notifyItemRangeRemoved(plus,posts.getConferenceNews().size()+plus);
                }
            });
            root_indicator2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(last_selected==1)
                        return;
                    last_selected=1;
                    conference=false;
                    indicator2.setVisibility(View.VISIBLE);
                    indicator1.setVisibility(View.INVISIBLE);
                    tab_text1.setTextColor(Color.parseColor(unSelected_tabColor));
                    tab_text2.setTextColor(Color.parseColor(selected_tabColor));
                    notifyItemRangeRemoved(plus,posts.getRacenetNews().size()+plus);
                }
            });
        }
    }

    class NewsViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.title)
        TextView title;
        @Bind(R.id.date)
        TextView date;
        @Bind(R.id.short_text)
        TextView short_text;
        @Bind(R.id.imageView7)
        ImageView image;
        @Bind(R.id.media_container)
        LinearLayout media_container;

        public NewsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            date.setTypeface(Aplication.lregular);
            title.setTypeface(Aplication.hlight);
            short_text.setTypeface(Aplication.hregular);
            Bitmap b= BitmapFactory.decodeResource(context.getResources(),R.drawable.generic_image_rectangular);
            float proportion= (float)b.getWidth()/b.getHeight();
            int alto= (int) ((Utils.getScreenWidth((Activity) context)-context.getResources().getDimension(R.dimen.image_margin))/proportion);
            LinearLayout.LayoutParams lp= (LinearLayout.LayoutParams)image.getLayoutParams();
            lp.height=alto;
            lp.width=Utils.getScreenWidth((Activity) context);
            image.setLayoutParams(lp);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(conference){
                        Intent detail= new Intent(context, NewsDetailActivity.class);
                        Bundle data= new Bundle();
                       // ConferenceNews c=posts.getConferenceNews().get(getAdapterPosition()-plus);
                        Aplication.newsList=posts;
                        data.putBoolean("conference",true);
                        data.putInt("pos",getAdapterPosition()-plus);
                        detail.putExtras(data);
                        context.startActivity(detail);
                    }else{
                        Intent detail= new Intent(context, NewsDetailActivity.class);
                        Bundle data= new Bundle();
                        //data.putSerializable("d",posts.getRacenetNews().get(getAdapterPosition()-plus));
                        Aplication.newsList=posts;
                        data.putBoolean("conference",false);
                        data.putInt("pos",getAdapterPosition()-plus);
                        detail.putExtras(data);
                        context.startActivity(detail);
                    }

                }
            });
        }
    }

    class TopViewPagerAdapter extends PagerAdapter{

        ViewPager pager;
        boolean isResize=false;
        int pagerW=0;
        int pagerH=0;
        View root;
        public TopViewPagerAdapter(ViewPager pager,RelativeLayout rootView) {
            this.pager = pager;
            this.root= rootView;
        }

        @Override
        public Object instantiateItem(final ViewGroup container, int position) {
            View view=((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.home_item_top_item, container, false);
            final Highlight highlightItem= posts.getHighlights().get(position);
            ((TextView)view.findViewById(R.id.textView6)).setTypeface(Aplication.hmedium);
            ((TextView)view.findViewById(R.id.textView6)).setText(highlightItem.getButtonText());
            ((TextView)view.findViewById(R.id.textView8)).setTypeface(Aplication.hthin);
            ((TextView)view.findViewById(R.id.textView8)).setText(highlightItem.getTitle());
            String url="";
            Bitmap b= BitmapFactory.decodeResource(context.getResources(),R.drawable.generic_image_rectangular);
            float proportion= (float)b.getWidth()/b.getHeight();
            int alto= (int) ((Utils.getScreenWidth((Activity) context))/proportion);
            RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams)((ImageView) view.findViewById(R.id.image)).getLayoutParams();
            if(isResize){
                lp.height=pagerH;
                lp.width=pagerW;
            }else {
                lp.height=alto;
                lp.width=Utils.getScreenWidth((Activity) context);
                pager.setLayoutParams(lp);
            }
            ((ImageView) view.findViewById(R.id.image)).setLayoutParams(lp);
           // ((ImageView) view.findViewById(R.id.imageView12)).setLayoutParams(lp);
            if(context.getResources().getBoolean(R.bool.is_tablet))
                url= ((LinkedTreeMap<String,String>)highlightItem.getMedia().get(0)).get("hi");
            else
                url=((LinkedTreeMap<String,String>)highlightItem.getMedia().get(0)).get("med");

            ImageLoader.getInstance().displayImage(url, (ImageView) view.findViewById(R.id.image), new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {
                    View parent= (View) view.getParent();
                    ((ImageView) parent.findViewById(R.id.imageView13)).setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                    int alto= (int) (Utils.getScreenWidth((Activity) context)/proportion);
                    RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) pager.getLayoutParams();
                    lp.height=alto;
                    lp.width=Utils.getScreenWidth((Activity) context);
                    view.setLayoutParams(lp);
                    view.setVisibility(View.VISIBLE);
                    View parent= (View) view.getParent();
                    ((ImageView) parent.findViewById(R.id.imageView13)).setVisibility(View.INVISIBLE);
                    if(!isResize){
                        pagerH=lp.height;
                        pagerW=lp.width;
                    pager.setLayoutParams(lp);
                    isResize=true;
                    }
                }

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });
            container.addView(view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (highlightItem.getType().toLowerCase().equals("http"))
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(highlightItem.getPathUrl())));
                    if(highlightItem.getType().toLowerCase().equals("article")){
                        RacenetNews c= new RacenetNews();
                        c.setText(highlightItem.getText());
                        c.setTitle(highlightItem.getTitle());
                        c.setMedia(highlightItem.getMedia());
                        Bundle bundle= new Bundle();
                        bundle.putSerializable("d",c);
                        Intent detail= new Intent(context, NewsHighLightDetailActivity.class);
                        detail.putExtras(bundle);
                        context.startActivity(detail);
                    }
                    else if(highlightItem.getType().toLowerCase().equals("go")) {
                    if(highlightItem.getPathUrl().toLowerCase().equals("partners")) {
                        ((HomeActivity) context).gotoSection(PartnerFragment.newInstance(((HomeActivity) context).getSettings().getMenuUrl(highlightItem.getType().toLowerCase()), "Partner"));
                        return;
                    }
                        if(highlightItem.getPathUrl().toLowerCase().equals("route")) {
                            ((HomeActivity) context).gotoSection(RuoteFragment.newInstance(((HomeActivity) context).getSettings().getMenuUrl(highlightItem.getType().toLowerCase()), "Route"));
                            return;
                        }
                        if(highlightItem.getPathUrl().toLowerCase().equals("teams")) {
                            ((HomeActivity) context).gotoSection(TeamFragment.newInstance(((HomeActivity) context).getSettings().getMenuUrl(highlightItem.getType().toLowerCase()), "Teams"));
                            return;
                        }
                        if(highlightItem.getPathUrl().toLowerCase().equals("whoiswho")) {
                            ((HomeActivity) context).gotoSection(WhoIsWhoFragment.newInstance(((HomeActivity) context).getSettings().getMenuUrl(highlightItem.getType().toLowerCase()), "WhoIsWho"));
                            return;
                        }
                        if(highlightItem.getPathUrl().toLowerCase().equals("conference-agenda") || highlightItem.getPathUrl().toLowerCase().equals("agenda")) {
                            ((HomeActivity) context).gotoSection(AgendaFragment.newInstance(((HomeActivity) context).getSettings().getMenuUrl(highlightItem.getType().toLowerCase()), "Conference Agenda"));
                            return;
                        }
                        if(highlightItem.getPathUrl().toLowerCase().equals("schedule")) {
                            ((HomeActivity) context).gotoSection(RaceScheduleFragment.newInstance(((HomeActivity) context).getSettings().getMenuUrl(highlightItem.getType().toLowerCase()), "Race Schedule"));
                            return;
                        }
                        if(highlightItem.getPathUrl().toLowerCase().equals("contact")) {
                            ((HomeActivity) context).gotoSection(ontactFragment.newInstance(((HomeActivity) context).getSettings().getMenuUrl(highlightItem.getType().toLowerCase()), "Contact"));
                            return;
                        }
                    }

                }
            });
            return view;
        }

        @Override
        public int getCount() {
            return posts.getHighlights().size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void destroyItem(View container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }
}
