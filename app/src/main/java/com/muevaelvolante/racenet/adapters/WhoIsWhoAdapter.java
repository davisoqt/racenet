package com.muevaelvolante.racenet.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.WhoIsWhoDetailActivity;
import com.muevaelvolante.racenet.model.who.Person;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Leo on 11/08/2016.
 */
public class WhoIsWhoAdapter extends RecyclerView.Adapter<WhoIsWhoAdapter.ViewHolder> implements Filterable{

    ArrayList<Person> data;
    ArrayList<Person> filtered;
    Context context;
    Filter filter;

    public void setData(ArrayList<Person> data) {
        this.data = data;
    }

    public WhoIsWhoAdapter(ArrayList<Person> data, Context context) {
        this.data = data;
        this.context = context;
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .showImageOnLoading(R.drawable.generic_image_square)
                .showImageOnFail(R.drawable.generic_image_square)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(Aplication.context)
                //.memoryCacheExtraOptions(480,480)
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().destroy();
        ImageLoader.getInstance().init(config);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.who_is_who_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        String url;
        Person data = WhoIsWhoAdapter.this.data.get(position);
        holder.name.setText(data.getFullname());
        holder.role.setText(data.getSubcategory());
        if (Aplication.is_teblet) {
            url = (data).getMedia().getHi();
        } else {
            url = data.getMedia().getMed();
        }
        ImageLoader.getInstance().displayImage(url, holder.image, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {

            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                Log.d("", "onLoadingComplete: ");
                if (bitmap != null) {
                    float proportion = (float) bitmap.getWidth() / bitmap.getHeight();
                    int alto = (int) ((Aplication.screenW / 2) / proportion);
                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    lp.height = alto;
                    lp.width = Aplication.screenW / 2;
                    view.setLayoutParams(lp);
                    holder.shadow.setLayoutParams(lp);
                    //((RelativeLayout)view.getParent()).setLayoutParams(lp);
                }
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public Filter getFilter() {
        if (filter==null )
            filter= new SimpleFilter();
        return filter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.imageView9)
        ImageView image;
        @Bind(R.id.textView13)
        TextView name;
        @Bind(R.id.textView14)
        TextView role;
        @Bind(R.id.imageView16)
        View shadow;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            name.setTypeface(Aplication.hlight);
            role.setTypeface(Aplication.hregular);
            Bitmap b= BitmapFactory.decodeResource(context.getResources(),R.drawable.generic_image_square2);
            float proportion = (float) b.getWidth() / b.getHeight();
            int alto = (int) ((Aplication.screenW / 2) / proportion);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
            lp.height = alto;
            lp.width = Aplication.screenW / 2;
            image.setLayoutParams(lp);
            shadow.setLayoutParams(lp);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent detail= new Intent(context, WhoIsWhoDetailActivity.class);
                    Bundle bundle= new Bundle();
                    bundle.putSerializable("p",data.get(getAdapterPosition()));
                    detail.putExtras(bundle);
                    context.startActivity(detail);
                }
            });
        }
    }

    class SimpleFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults results = new FilterResults();

            if (filtered == null) {
                filtered = new ArrayList<Person>(data);
            }

            if (charSequence == null || charSequence.length() == 0) {
                ArrayList<Person> list = filtered;
                results.values = list;
                results.count = list.size();
            } else {
                String prefixString = charSequence.toString().toLowerCase();
                //prefixString=prefixString.replace(" ","");
                ArrayList<Person> unfilteredValues = filtered;
                int count = unfilteredValues.size();
                ArrayList<Person> newValues = new ArrayList<Person>();
                prefixString=prefixString.replace("á", "a").replace("é","e").replace("í","i").replace("ó","o").replace("ú","u");
                for (int i = 0; i < count; i++) {
                    Person model = unfilteredValues.get(i);
                    if (model != null) {
//                        String str = model.getName().toLowerCase()+model.getLastName().toLowerCase()+model.getEmail().toLowerCase();
//                        str=str.replace("á","a").replace("é","e").replace("í","i").replace("ó","o").replace("ú","u");
                        if(model.getFullname().toLowerCase().contains(prefixString)){
                            newValues.add(model);
                            continue;
                        }
                    }
                }
                results.values = newValues;
                results.count = newValues.size();
           }

            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            data= (ArrayList<Person>) filterResults.values;
            notifyDataSetChanged();
        }
    }
}
