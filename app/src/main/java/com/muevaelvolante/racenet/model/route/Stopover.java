
package com.muevaelvolante.racenet.model.route;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Stopover implements Serializable{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("dates")
    @Expose
    private String dates;
    @SerializedName("text")
    @Expose
    private List<Object> text = new ArrayList<Object>();
    @SerializedName("media")
    @Expose
    private List<Object> media = new ArrayList<Object>();

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The dates
     */
    public String getDates() {
        return dates;
    }

    /**
     * 
     * @param dates
     *     The dates
     */
    public void setDates(String dates) {
        this.dates = dates;
    }

    /**
     * 
     * @return
     *     The text
     */
    public List<Object> getText() {
        return text;
    }

    /**
     * 
     * @param text
     *     The text
     */
    public void setText(List<Object> text) {
        this.text = text;
    }

    /**
     * 
     * @return
     *     The media
     */
    public List<Object> getMedia() {
        return media;
    }

    /**
     * 
     * @param media
     *     The media
     */
    public void setMedia(List<Object> media) {
        this.media = media;
    }

}
