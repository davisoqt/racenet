
package com.muevaelvolante.racenet.model.setting;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class PushNotification {

    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Channel")
    @Expose
    private String channel;

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The Name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The channel
     */
    public String getChannel() {
        return channel;
    }

    /**
     * 
     * @param channel
     *     The Channel
     */
    public void setChannel(String channel) {
        this.channel = channel;
    }

}
