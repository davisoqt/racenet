
package com.muevaelvolante.racenet.model.contact;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Contact {

    @SerializedName("Contact")
    @Expose
    private List<Contact_> contact = new ArrayList<Contact_>();

    /**
     * 
     * @return
     *     The contact
     */
    public List<Contact_> getContact() {
        return contact;
    }

    /**
     * 
     * @param contact
     *     The Contact
     */
    public void setContact(List<Contact_> contact) {
        this.contact = contact;
    }

}
