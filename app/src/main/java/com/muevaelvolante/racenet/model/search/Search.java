
package com.muevaelvolante.racenet.model.search;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.muevaelvolante.racenet.model.post.ConferenceNews;
import com.muevaelvolante.racenet.model.post.RacenetNews;

@Generated("org.jsonschema2pojo")
public class Search {

    @SerializedName("conference news")
    @Expose
    private ArrayList<ConferenceNews> conferenceNews = new ArrayList<ConferenceNews>();
    @SerializedName("racenet news")
    @Expose
    private ArrayList<RacenetNews> racenetNews = new ArrayList<RacenetNews>();

    /**
     * 
     * @return
     *     The conferenceNews
     */
    public ArrayList<ConferenceNews> getConferenceNews() {
        return conferenceNews;
    }

    /**
     * 
     * @param conferenceNews
     *     The conference news
     */
    public void setConferenceNews(ArrayList<ConferenceNews> conferenceNews) {
        this.conferenceNews = conferenceNews;
    }

    /**
     * 
     * @return
     *     The racenetNews
     */
    public ArrayList<RacenetNews> getRacenetNews() {
        return racenetNews;
    }

    /**
     * 
     * @param racenetNews
     *     The racenet news
     */
    public void setRacenetNews(ArrayList<RacenetNews> racenetNews) {
        this.racenetNews = racenetNews;
    }

}
