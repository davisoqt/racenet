
package com.muevaelvolante.racenet.model.post;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Post {

    @SerializedName("highlights")
    @Expose
    private List<Highlight> highlights = new ArrayList<Highlight>();
    @SerializedName("conference news")
    @Expose
    private ArrayList<ConferenceNews> conferenceNews = new ArrayList<ConferenceNews>();
    @SerializedName("racenet news")
    @Expose
    private ArrayList<RacenetNews> racenetNews = new ArrayList<RacenetNews>();
    @SerializedName("categories")
    @Expose
    private List<Category> categories = new ArrayList<Category>();

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    /**
     * 
     * @return
     *     The highlights
     */
    public List<Highlight> getHighlights() {
        return highlights;
    }

    /**
     * 
     * @param highlights
     *     The highlights
     */
    public void setHighlights(List<Highlight> highlights) {
        this.highlights = highlights;
    }

    /**
     * 
     * @return
     *     The conferenceNews
     */
    public ArrayList<ConferenceNews> getConferenceNews() {
        return conferenceNews;
    }

    /**
     * 
     * @param conferenceNews
     *     The conference news
     */
    public void setConferenceNews(ArrayList<ConferenceNews> conferenceNews) {
        this.conferenceNews = conferenceNews;
    }

    /**
     * 
     * @return
     *     The racenetNews
     */
    public ArrayList<RacenetNews> getRacenetNews() {
        return racenetNews;
    }

    /**
     * 
     * @param racenetNews
     *     The racenet news
     */
    public void setRacenetNews(ArrayList<RacenetNews> racenetNews) {
        this.racenetNews = racenetNews;
    }

}
