
package com.muevaelvolante.racenet.model.team;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import butterknife.Bind;
import butterknife.ButterKnife;

@Generated("org.jsonschema2pojo")
public class Team_ extends AbstractItem<Team_,Team_.ViewHolder> implements Serializable{

    @SerializedName("team_name")
    @Expose
    private String teamName;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("skipper")
    @Expose
    private String skipper;
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("text")
    @Expose
    private List<Object> text = new ArrayList<Object>();
    @SerializedName("media")
    @Expose
    private List<Medium> media = new ArrayList<Medium>();

    /**
     * 
     * @return
     *     The teamName
     */
    public String getTeamName() {
        return teamName;
    }

    /**
     * 
     * @param teamName
     *     The team_name
     */
    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    /**
     * 
     * @return
     *     The country
     */
    public String getCountry() {
        return country;
    }

    /**
     * 
     * @param country
     *     The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * 
     * @return
     *     The skipper
     */
    public String getSkipper() {
        return skipper;
    }

    /**
     * 
     * @param skipper
     *     The skipper
     */
    public void setSkipper(String skipper) {
        this.skipper = skipper;
    }

    /**
     * 
     * @return
     *     The nationality
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * 
     * @param nationality
     *     The nationality
     */
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    /**
     * 
     * @return
     *     The text
     */
    public List<Object> getText() {
        return text;
    }

    /**
     * 
     * @param text
     *     The text
     */
    public void setText(List<Object> text) {
        this.text = text;
    }

    /**
     * 
     * @return
     *     The media
     */
    public List<Medium> getMedia() {
        return media;
    }

    /**
     * 
     * @param media
     *     The media
     */
    public void setMedia(List<Medium> media) {
        this.media = media;
    }

    @Override
    public int getType() {
        return 0;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_team_item;
    }

    @Override
    public void bindView(ViewHolder holder) {
        super.bindView(holder);
        String url_bg;
        String url_ico;
        if(Aplication.context.getResources().getBoolean(R.bool.is_tablet)){
            url_bg= getMedia().get(1).getHi();
            url_ico=getMedia().get(0).getHi();
        }else{
            url_bg= getMedia().get(1).getMed();
            url_ico=getMedia().get(0).getMed();
        }
        holder.title.setText(getTeamName());
        ImageLoader.getInstance().displayImage(url_ico,holder.icon);
        ImageLoader.getInstance().displayImage(url_bg, holder.bg, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {

            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                int alto= (int) (Aplication.screenW/proportion);
                RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams)view.getLayoutParams();
                lp.height=alto;
                lp.width=Aplication.screenW;
                view.setLayoutParams(lp);
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.bg)
        ImageView bg;
        @Bind(R.id.icon)
        ImageView icon;
        @Bind(R.id.title)
        TextView title;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            Bitmap b= BitmapFactory.decodeResource(Aplication.context.getResources(),R.drawable.generic_image_rectangular);
            float proportion = (float) b.getWidth() / b.getHeight();
            int alto = (int) ((Aplication.screenW) / proportion);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) bg.getLayoutParams();
            lp.height = alto;
            lp.width = Aplication.screenW;
            bg.setLayoutParams(lp);
            title.setTypeface(Aplication.hlight);

        }
    }
}
