
package com.muevaelvolante.racenet.model.who;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class People {

    @SerializedName("people")
    @Expose
    private ArrayList<Person> people = new ArrayList<Person>();
    @SerializedName("categories")
    @Expose
    private List<Category> categories = new ArrayList<Category>();

    /**
     * 
     * @return
     *     The people
     */
    public ArrayList<Person> getPeople() {
        return people;
    }

    /**
     * 
     * @param people
     *     The people
     */
    public void setPeople(ArrayList<Person> people) {
        this.people = people;
    }

    /**
     * 
     * @return
     *     The categories
     */
    public List<Category> getCategories() {
        return categories;
    }

    /**
     * 
     * @param categories
     *     The categories
     */
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public ArrayList<String> getSubCategory(String categoryName){
        ArrayList<String> data= new ArrayList<>();
        data.add(categoryName+" (ALL)");
        for (Category c: getCategories()) {
            if(c.getName().equals(categoryName))
                for (Subcategory s :
                        c.getSubcategories()) {
                    data.add(s.getName());
                }
        }
        return data;
    }

    public ArrayList<String> getAllCategories(){
        ArrayList<String> data= new ArrayList<>();
        for (Category c :
                getCategories()) {
            data.add(c.getName());
        }return data;
    }

    public ArrayList<Person> getPersonForSubCategories(String subC){
        ArrayList<Person> data= new ArrayList<>();
        for (Person p :
                getPeople()) {
            if (p.getSubcategory().equals(subC))
                data.add(p);
        }
        return data;
    }

    public ArrayList<Person> getpersonForCategory(String cat){
        ArrayList<Person> data= new ArrayList<>();
        for (Person p :
                getPeople()) {
            if (p.getCategory().equals(cat))
                data.add(p);
        }
        return data;
    }

}
