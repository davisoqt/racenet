
package com.muevaelvolante.racenet.model.post;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class ConferenceNews implements Parcelable, Serializable{

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("short_text")
    @Expose
    private String shortText;
    @SerializedName("text")
    @Expose
    private List<Object> text = new ArrayList<Object>();
    @SerializedName("media")
    @Expose
    private List<Object> media = new ArrayList<Object>();

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The date
     */
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The shortText
     */
    public String getShortText() {
        return shortText;
    }

    /**
     * 
     * @param shortText
     *     The short_text
     */
    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    /**
     * 
     * @return
     *     The text
     */
    public List<Object> getText() {
        return text;
    }

    /**
     * 
     * @param text
     *     The text
     */
    public void setText(List<Object> text) {
        this.text = text;
    }

    /**
     * 
     * @return
     *     The media
     */
    public List<Object> getMedia() {
        return media;
    }

    /**
     * 
     * @param media
     *     The media
     */
    public void setMedia(List<Object> media) {
        this.media = media;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(getMedia());
        dest.writeList(getText());
        dest.writeString(getShortText());
        dest.writeString(getTitle());
        dest.writeString(getDate());
        dest.writeString(getType());
    }


}
