
package com.muevaelvolante.racenet.model.agenda;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Home implements Serializable{

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("dates")
    @Expose
    private String dates;
    @SerializedName("short_text")
    @Expose
    private String shortText;
    @SerializedName("quote")
    @Expose
    private String quote;
    @SerializedName("media")
    @Expose
    private List<Medium> media = new ArrayList<Medium>();

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The dates
     */
    public String getDates() {
        return dates;
    }

    /**
     * 
     * @param dates
     *     The dates
     */
    public void setDates(String dates) {
        this.dates = dates;
    }

    /**
     * 
     * @return
     *     The shortText
     */
    public String getShortText() {
        return shortText;
    }

    /**
     * 
     * @param shortText
     *     The short_text
     */
    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    /**
     * 
     * @return
     *     The quote
     */
    public String getQuote() {
        return quote;
    }

    /**
     * 
     * @param quote
     *     The quote
     */
    public void setQuote(String quote) {
        this.quote = quote;
    }

    /**
     * 
     * @return
     *     The media
     */
    public List<Medium> getMedia() {
        return media;
    }

    /**
     * 
     * @param media
     *     The media
     */
    public void setMedia(List<Medium> media) {
        this.media = media;
    }

}
