
package com.muevaelvolante.racenet.model.route;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Route {

    @SerializedName("stopovers")
    @Expose
    private List<Stopover> stopovers = new ArrayList<Stopover>();
    @SerializedName("legs")
    @Expose
    private List<Stopover> legs = new ArrayList<Stopover>();

    /**
     * 
     * @return
     *     The stopovers
     */
    public List<Stopover> getStopovers() {
        return stopovers;
    }

    /**
     * 
     * @param stopovers
     *     The stopovers
     */
    public void setStopovers(List<Stopover> stopovers) {
        this.stopovers = stopovers;
    }

    /**
     * 
     * @return
     *     The legs
     */
    public List<Stopover> getLegs() {
        return legs;
    }

    /**
     * 
     * @param legs
     *     The legs
     */
    public void setLegs(List<Stopover> legs) {
        this.legs = legs;
    }

}
