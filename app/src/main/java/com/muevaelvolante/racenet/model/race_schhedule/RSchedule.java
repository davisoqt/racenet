
package com.muevaelvolante.racenet.model.race_schhedule;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class RSchedule {

    @SerializedName("race schedule")
    @Expose
    private List<RaceSchedule> raceSchedule = new ArrayList<RaceSchedule>();

    /**
     * 
     * @return
     *     The raceSchedule
     */
    public List<RaceSchedule> getRaceSchedule() {
        return raceSchedule;
    }

    /**
     * 
     * @param raceSchedule
     *     The race schedule
     */
    public void setRaceSchedule(List<RaceSchedule> raceSchedule) {
        this.raceSchedule = raceSchedule;
    }

}
