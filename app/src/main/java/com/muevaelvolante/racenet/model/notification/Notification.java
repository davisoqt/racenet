
package com.muevaelvolante.racenet.model.notification;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Notification {

    @SerializedName("push-notifications intro text")
    @Expose
    private List<PushNotificationsIntroText> pushNotificationsIntroText = new ArrayList<PushNotificationsIntroText>();
    @SerializedName("push-notifications")
    @Expose
    private List<PushNotification> pushNotifications = new ArrayList<PushNotification>();

    /**
     * 
     * @return
     *     The pushNotificationsIntroText
     */
    public List<PushNotificationsIntroText> getPushNotificationsIntroText() {
        return pushNotificationsIntroText;
    }

    /**
     * 
     * @param pushNotificationsIntroText
     *     The push-notifications intro text
     */
    public void setPushNotificationsIntroText(List<PushNotificationsIntroText> pushNotificationsIntroText) {
        this.pushNotificationsIntroText = pushNotificationsIntroText;
    }

    /**
     * 
     * @return
     *     The pushNotifications
     */
    public List<PushNotification> getPushNotifications() {
        return pushNotifications;
    }

    /**
     * 
     * @param pushNotifications
     *     The push-notifications
     */
    public void setPushNotifications(List<PushNotification> pushNotifications) {
        this.pushNotifications = pushNotifications;
    }

}
