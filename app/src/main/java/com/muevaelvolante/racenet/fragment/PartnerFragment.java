package com.muevaelvolante.racenet.fragment;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.HomeActivity;
import com.muevaelvolante.racenet.adapters.PartnerItemAdapter;
import com.muevaelvolante.racenet.adapters.PatnersAdapter;
import com.muevaelvolante.racenet.model.partner.Partner;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;


public class PartnerFragment extends RootFragment implements HomeActivity.ToolBarTitleAction {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.list)
    RecyclerView partners_list;
    @Bind(R.id.list2)
    RecyclerView partners_details;
    @Bind(R.id.textView18)
    TextView partner_selected;
    private LinkedTreeMap<String, ArrayList<LinkedTreeMap>> json;
    List<Object> allPartner = new ArrayList<>();
    private ArrayList partners;

    public PartnerItemAdapter getItem_adapter() {
        return item_adapter;
    }

    private PartnerItemAdapter item_adapter;
    HomeActivity homeActivity;

    public PartnerFragment() {
        // Required empty public constructor
    }

    public static PartnerFragment newInstance(String param1, String param2) {
        PartnerFragment fragment = new PartnerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString("t", param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uri = getArguments().getString(ARG_PARAM1);
            json_name = "partner";
            title_name = getArguments().getString("t");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_partner, container, false);
        ButterKnife.bind(this, layout);
        partner_selected.setTypeface(Aplication.hlight);
        return layout;
    }

  @Override
    public void proccessData(Object data) {
        Gson gson = new Gson();
        json = gson.fromJson((String) data, LinkedTreeMap.class);
        //obteemos los partners
        partners = new ArrayList();
        partners.add("ALL PARTNERS");
        for (Map.Entry<String, ArrayList<LinkedTreeMap>> entry_set : json.entrySet()) {
            partners.add(entry_set.getKey());
            //allPartner.add(entry_set.getKey());
            allPartner.addAll(getPartnerForKey(entry_set.getKey()));
        }
        partners_list.setAdapter(new PatnersAdapter(partners, this));
        partners_list.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getContext()).color(Color.parseColor("#ffffff")).build());
        //habilitamos el primero por defecto
        //allPartner.addAll(getPartnerForKey((String) partners.get(0)));
        item_adapter = new PartnerItemAdapter(allPartner, getActivity(),"",this);
        setPartnerSelected(partners.get(0).toString());
        partners_details.setAdapter(item_adapter);
        partners_details.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getContext()).color(Color.parseColor("#d5d5db")).build());
        hideDialog();
    }

    public void setPartnerSelected( String partner_name){
        partner_selected.setText(partner_name);
    }

    public String getPartnerSelected(){
        return partner_selected.getText().toString();
    }

    public List<Object> getPartnerForKey(String key) {
        List<Object> list = new ArrayList<>();
        if(key.equals("ALL PARTNERS")){
            for(int i=1;i< partners.size(); i++){
                //list.add(partners.get(i));
                list.addAll(getPartnerForKey((String) partners.get(i)));
            }
            return list;
        }
        ArrayList<LinkedTreeMap> data = json.get(key);
        list.add(key);
        for (int i = 0; i < data.size(); i++) {
            Partner partner=new Partner((String) data.get(i).get("url"), (String) data.get(i).get("name"), (List<Object>) data.get(i).get("text"), (List<Object>) data.get(i).get("media"));
            partner.setCategory(key);
            list.add(partner);
        }
        return list;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        homeActivity.spinnerOff();
    }

    public void closePartnersList() {
        partners_list.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.slide_top_out));
        partners_list.setVisibility(View.GONE);
    }

    @Override
    public void toolBarClick() {
        if (partners_list.getVisibility() == View.VISIBLE) {
            partners_list.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.slide_top_out));
            partners_list.setVisibility(View.GONE);
        } else {
            partners_list.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.slide_top_in));
            partners_list.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        homeActivity = (HomeActivity) context;
        homeActivity.spinnerOn();
        ((HomeActivity)context).hideSearch();
    }
}
