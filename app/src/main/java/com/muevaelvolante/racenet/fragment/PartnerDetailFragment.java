package com.muevaelvolante.racenet.fragment;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.internal.LinkedTreeMap;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.VideoActivity;
import com.muevaelvolante.racenet.model.partner.Partner;
import com.muevaelvolante.racenet.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PartnerDetailFragment extends Fragment {
    
    private static final String ARG_PARAM1 = "param1";
    String categoty;
    @Bind(R.id.partner_cat)
    TextView title;
    @Bind(R.id.image)
    ImageView image;
    @Bind(R.id.container)
    LinearLayout container;
    private Partner data;
    private LayoutInflater inflater;

    public PartnerDetailFragment() {
        // Required empty public constructor
    }

    public static PartnerDetailFragment newInstance(Partner param1, String param2) {
        PartnerDetailFragment fragment = new PartnerDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, param1);
        args.putString("cat",param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            data = (Partner) getArguments().getSerializable(ARG_PARAM1);
            categoty=getArguments().getString("cat");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containerr,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout= inflater.inflate(R.layout.fragment_partner_detail, container, false);
        ButterKnife.bind(this,layout);
        this.inflater= inflater;
        String url;
        if (Aplication.is_teblet) {
            url = ((LinkedTreeMap<String, String>) (data).getMedia().get(0)).get("hi");
        } else {
            url = ((LinkedTreeMap<String, String>) data.getMedia().get(0)).get("med");
        }
        Bitmap bb= BitmapFactory.decodeResource(Aplication.context.getResources(),R.drawable.generic_image_rectangular);
        float proportionn = (float) bb.getWidth() / bb.getHeight();
        int altoo = (int) ((Aplication.screenW) / proportionn);
        RelativeLayout.LayoutParams lpp = (RelativeLayout.LayoutParams) image.getLayoutParams();
        lpp.height = altoo;
        lpp.width = Aplication.screenW;
        image.setLayoutParams(lpp);
        ImageLoader.getInstance().displayImage(url, image, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {

            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                Log.d("", "onLoadingComplete: ");
                float proportion = (float) bitmap.getWidth() / bitmap.getHeight();
                int alto = (int) (Aplication.screenW / proportion);
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
                lp.height = alto;
                lp.width = Aplication.screenW;
                view.setLayoutParams(lp);
                //((RelativeLayout)view.getParent()).setLayoutParams(lp);
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });
        title.setTypeface(Aplication.hlight);
        title.setText(data.getCategory());
        List<Object> texts=data.getText();
        for (int i = 0; i < texts.size(); i++) {
            Object item = texts.get(i);
            if (item instanceof String) {
                View string = inflater.inflate(R.layout.news_text, null);
                ((TextView) string.findViewById(R.id.text)).setTypeface(Aplication.hregular);
                ((TextView) string.findViewById(R.id.text)).setText(Html.fromHtml(String.valueOf(item)));
                container.addView(string);
                continue;
            }
            if (item instanceof LinkedTreeMap) {
                {
                    LinkedTreeMap<String, String> map = (LinkedTreeMap<String, String>) item;
                    if (map.size() == 3) { //es una imagen
                        View imagen = inflater.inflate(R.layout.news_image, null);
                        final ImageView image = (ImageView) imagen.findViewById(R.id.image);
                        Bitmap b= BitmapFactory.decodeResource(Aplication.context.getResources(),R.drawable.generic_image_rectangular);
                        float proportion = (float) b.getWidth() / b.getHeight();
                        int alto = (int) ((Aplication.screenW) / proportion);
                        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
                        lp.height = alto;
                        lp.width = Aplication.screenW;
                        image.setLayoutParams(lp);
                        ImageLoader.getInstance().displayImage(map.get("med"), image, new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String s, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String s, View view, FailReason failReason) {

                            }

                            @Override
                            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                                float proportion = (float) bitmap.getWidth() / bitmap.getHeight();
                                int alto = (int) (Aplication.screenW / proportion);
                                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
                                lp.height = alto;
                                lp.width = Utils.getScreenWidth(getActivity());
                                image.setLayoutParams(lp);
                            }

                            @Override
                            public void onLoadingCancelled(String s, View view) {

                            }
                        });
                        container.addView(imagen);
                        continue;
                    }
                    if (map.size() == 4) {//es un video
                        final View video = inflater.inflate(R.layout.news_video, null);
                        final ImageView video_image = ((ImageView) video.findViewById(R.id.video_image));
                        Bitmap b= BitmapFactory.decodeResource(Aplication.context.getResources(),R.drawable.generic_image_rectangular);
                        float proportion = (float) b.getWidth() / b.getHeight();
                        int alto = (int) ((Aplication.screenW) / proportion);
                        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) video_image.getLayoutParams();
                        lp.height = alto;
                        lp.width = Aplication.screenW;
                        video_image.setLayoutParams(lp);
                        final String video_url = map.get("video");
                        ImageLoader.getInstance().displayImage(map.get("med"), video_image, new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String s, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String s, View view, FailReason failReason) {

                            }

                            @Override
                            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                                float proportion = (float) bitmap.getWidth() / bitmap.getHeight();
                                int alto = (int) (Utils.getScreenWidth(getActivity()) / proportion);
                                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) video_image.getLayoutParams();
                                lp.height = alto;
                                lp.width = Utils.getScreenWidth(getActivity());
                                video_image.setLayoutParams(lp);
                            }

                            @Override
                            public void onLoadingCancelled(String s, View view) {

                            }
                        });
                        video.findViewById(R.id.play).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (!video_url.contains("youtube")) {
                                    Intent video = new Intent(getActivity(), VideoActivity.class);
                                    video.putExtra("uri", video_url);
                                    startActivity(video);
                                } else {
                                    Utils.playVideoYoutube(getActivity(), video_url);
                                }
                            }
                        });
                        container.addView(video);
                    }
                }
            }
            if(item instanceof ArrayList){//slide
                View galeria = inflater.inflate(R.layout.news_gallery, null);
                ViewPager pager = (ViewPager) galeria.findViewById(R.id.pager);
                GalleryAdapter galleryAdapter = new GalleryAdapter(pager, (ArrayList<LinkedTreeMap<String, String>>) item);
                pager.setAdapter(galleryAdapter);
                container.addView(galeria);
            }
        }
        View end = inflater.inflate(R.layout.partner_detail_end, null);
        container.addView(end);
        ((TextView)end.findViewById(R.id.url)).setText(data.getUrl());
        end.findViewById(R.id.url).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+data.getUrl())));
            }
        });
        return layout;
    }

    class GalleryAdapter extends PagerAdapter {


        @Bind(R.id.image)
        ImageView image;
        @Bind(R.id.next)
        View next;
        @Bind(R.id.prev)
        View prev;
        ViewPager pager;
        boolean isViewpagerResized = false;

        ArrayList<LinkedTreeMap<String, String>> items;


        public GalleryAdapter(ViewPager pager, ArrayList<LinkedTreeMap<String, String>> items) {
            this.pager = pager;
            this.items = items;
            Bitmap b= BitmapFactory.decodeResource(Aplication.context.getResources(),R.drawable.generic_image_rectangular);
            float proportion = (float) b.getWidth() / b.getHeight();
            int alto = (int) ((Aplication.screenW) / proportion);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) pager.getLayoutParams();
            lp.height = alto;
            lp.width = Aplication.screenW;
            pager.setLayoutParams(lp);
        }

        @Override
        public Object instantiateItem(final ViewGroup container, int position) {
            final LinkedTreeMap<String, String> galleryItem = items.get(position);
            View view = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.news_gallery_item, container, false);
            ButterKnife.bind(this, view);
            ImageLoader.getInstance().displayImage(Uri.parse(galleryItem.get("med")).toString(), image, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {

                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    float proportion = (float) bitmap.getWidth() / bitmap.getHeight();
                    int alto = (int) (Utils.getScreenWidth(getActivity()) / proportion);
                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
                    lp.height = alto;
                    lp.width = Utils.getScreenWidth(getActivity());
                    image.setLayoutParams(lp);
                    if (!isViewpagerResized) {
                        pager.setLayoutParams(lp);
                        isViewpagerResized = true;
                    }
                }

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });

            if (position < items.size() - 1) {
                next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (pager.getCurrentItem() + 1 <= items.size() - 1)
                            pager.setCurrentItem(pager.getCurrentItem() + 1);
                    }
                });
            } else
                next.setVisibility(View.GONE);
            if (position > 0) {
                prev.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (pager.getCurrentItem() - 1 >= 0)
                            pager.setCurrentItem(pager.getCurrentItem() - 1);
                    }
                });
            } else
                prev.setVisibility(View.GONE);
            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void destroyItem(View container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }
}
