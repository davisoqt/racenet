package com.muevaelvolante.racenet.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amazonaws.util.LengthCheckInputStream;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.adapters.SpeakersAdapter;
import com.muevaelvolante.racenet.model.agenda.Speaker;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SpeakersFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @Bind(R.id.list)
    RecyclerView recyclerView;
    ArrayList<Speaker> speakers;
    @Bind(R.id.category_selected)
    View no_visible;
    public SpeakersFragment() {
        // Required empty public constructor
    }

    public static SpeakersFragment newInstance(ArrayList<Speaker> param1, String param2) {
        SpeakersFragment fragment = new SpeakersFragment();
        Bundle args = new Bundle();
        args.putSerializable("d", param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            speakers = (ArrayList<Speaker>) getArguments().getSerializable("d");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout= inflater.inflate(R.layout.fragment_who_is_who, container, false);
        ButterKnife.bind(this,layout);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        recyclerView.setAdapter(new SpeakersAdapter(speakers,getActivity()));
        no_visible.setVisibility(View.GONE);
        return layout;
    }

}
