package com.muevaelvolante.racenet.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.HomeActivity;
import com.muevaelvolante.racenet.activity.TeamDetailActivity;
import com.muevaelvolante.racenet.model.team.Team;
import com.muevaelvolante.racenet.model.team.Team_;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TeamFragment extends RootFragment {

    private static final String ARG_PARAM1 = "param1";
    private OnFragmentInteractionListener mListener;
    @Bind(R.id.list)
    RecyclerView recyclerView;

    public TeamFragment() {
        // Required empty public constructor
    }

    @Override
    public void proccessData(Object data) {
        Team team= new Gson().fromJson((String) data,Team.class);
        FastItemAdapter<Team_> adapter= new FastItemAdapter<>();
        recyclerView.setAdapter(adapter);
        adapter.withOnClickListener(new FastAdapter.OnClickListener<Team_>() {
            @Override
            public boolean onClick(View v, IAdapter<Team_> adapter, Team_ item, int position) {
                Intent detail= new Intent(getContext(), TeamDetailActivity.class);
                Bundle data= new Bundle();
                data.putSerializable("d",adapter.getAdapterItem(position));
                Aplication.teamList=adapter.getAdapterItems();
                data.putInt("pos",position);
                detail.putExtras(data);
                getContext().startActivity(detail);
                return true;
            }
        });
        adapter.add(team.getTeams());
        hideDialog();
    }

    public static TeamFragment newInstance(String uri, String param2) {
        TeamFragment fragment = new TeamFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, uri);
        args.putString("t",param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uri = getArguments().getString(ARG_PARAM1);
            json_name="team";
            title_name=getArguments().getString("t");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout= inflater.inflate(R.layout.fragment_team, container, false);
        ButterKnife.bind(this,layout);
        return layout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((HomeActivity)context).hideSearch();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
