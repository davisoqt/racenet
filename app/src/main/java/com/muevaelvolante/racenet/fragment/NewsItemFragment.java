package com.muevaelvolante.racenet.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.internal.LinkedTreeMap;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.ReaderActivity;
import com.muevaelvolante.racenet.activity.VideoActivity;
import com.muevaelvolante.racenet.model.post.ConferenceNews;
import com.muevaelvolante.racenet.model.post.RacenetNews;
import com.muevaelvolante.racenet.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class NewsItemFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    Object data;
    @Bind(R.id.date)
    TextView date;
    @Bind(R.id.title)
    TextView title;
    @Bind(R.id.image)
    ImageView image;
    @Bind(R.id.container)
    LinearLayout container;
    @Bind(R.id.play)
    View play;
    private LayoutInflater inflater;

    public NewsItemFragment() {
        // Required empty public constructor
    }

    public static NewsItemFragment newInstance(Object param1) {
        NewsItemFragment fragment = new NewsItemFragment();
        Bundle args = new Bundle();
        if(param1 instanceof ConferenceNews)
        args.putSerializable(ARG_PARAM1, (ConferenceNews)param1);
        else
            args.putSerializable(ARG_PARAM1, (RacenetNews)param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            data = getArguments().getSerializable(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containerr,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_blank, container, false);
        this.inflater = inflater;
        ButterKnife.bind(this, layout);
        date.setTypeface(Aplication.lregular);
        title.setTypeface(Aplication.hlight);
        List<Object> texts;
        List<Object> media;
        String url;
        if (data instanceof ConferenceNews) {
            media= ((ConferenceNews)data).getMedia();
            date.setText(Utils.getDateString(Utils.convertStringToDate(((ConferenceNews) data).getDate())));
            date.setVisibility(View.VISIBLE);
            title.setText(((ConferenceNews) data).getTitle());
            texts = ((ConferenceNews) data).getText();
            int position = 0;
            String video_url = null;
            for (int i = 0; i < ((ConferenceNews) data).getMedia().size(); i++) {
                if (((LinkedTreeMap<String, String>) ((ConferenceNews) data).getMedia().get(i)).size() == 4) {
                    position = i;
                    video_url = ((LinkedTreeMap<String, String>) ((ConferenceNews) data).getMedia().get(i)).get("video");
                    play.setTag(video_url);
                    break;
                }
            }
            if (Aplication.is_teblet) {
                url = ((LinkedTreeMap<String, String>) ((ConferenceNews) data).getMedia().get(position)).get("hi");
            } else {
                url = ((LinkedTreeMap<String, String>) ((ConferenceNews) data).getMedia().get(position)).get("med");
            }
            if (video_url != null) {
                play.setVisibility(View.VISIBLE);
                play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String dir = (String) view.getTag();
                        if (!dir.contains("youtube")) {
                            Intent video = new Intent(getActivity(), VideoActivity.class);
                            video.putExtra("uri", dir);
                            startActivity(video);
                        } else {
                            Utils.playVideoYoutube(getActivity(), dir);
                        }
                    }
                });
            }

        } else {
            media= ((RacenetNews)data).getMedia();
            if (((RacenetNews) data).getDate() != null) {
                date.setText(Utils.getDateString(Utils.convertStringToDate(((RacenetNews) data).getDate())));
                date.setVisibility(View.VISIBLE);
            }
            title.setText(((RacenetNews) data).getTitle());
            texts = ((RacenetNews) data).getText();
            int position = 0;
            String video_url = null;
            for (int i = 0; i < ((RacenetNews) data).getMedia().size(); i++) {
                if (((LinkedTreeMap<String, String>) ((RacenetNews) data).getMedia().get(i)).size() == 4) {
                    position = i;
                    video_url = ((LinkedTreeMap<String, String>) ((RacenetNews) data).getMedia().get(i)).get("video");
                    play.setTag(video_url);
                    break;
                }
            }
            if (Aplication.is_teblet) {
                url = ((LinkedTreeMap<String, String>) ((RacenetNews) data).getMedia().get(position)).get("hi");
            } else {
                url = ((LinkedTreeMap<String, String>) ((RacenetNews) data).getMedia().get(position)).get("med");
            }
            if (video_url != null) {
                play.setVisibility(View.VISIBLE);
                play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String dir = (String) view.getTag();
                        if (!dir.contains("youtube")) {
                            Intent video = new Intent(getActivity(), VideoActivity.class);
                            video.putExtra("uri", dir);
                            startActivity(video);
                        } else {
                            Utils.playVideoYoutube(getActivity(), dir);
                        }
                    }
                });
            }
        }
        if (url == null)
            image.setVisibility(View.GONE);
        else
            ImageLoader.getInstance().displayImage(url, image, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {

                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    float proportion = (float) bitmap.getWidth() / bitmap.getHeight();
                    int alto = (int) ((Utils.getScreenWidth(getActivity())) / proportion);
                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    lp.height = alto;
                    lp.width = Utils.getScreenWidth(getActivity());
                    view.setLayoutParams(lp);
                }

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });

        for (int i = 0; i < texts.size(); i++) {
            Object item = texts.get(i);
            if (item instanceof String) {
                View string = inflater.inflate(R.layout.news_text, null);
                ((TextView) string.findViewById(R.id.text)).setTypeface(Aplication.hregular);
                ((TextView) string.findViewById(R.id.text)).setMovementMethod(new LinkMovementMethod());
                ((TextView) string.findViewById(R.id.text)).setText(Html.fromHtml(String.valueOf(item)));
                container.addView(string);
                continue;
            }
            if (item instanceof LinkedTreeMap) {
                {
                    LinkedTreeMap<String, String> map = (LinkedTreeMap<String, String>) item;
                    if (map.size() == 3) { //es una imagen
                        View imagen = inflater.inflate(R.layout.news_image, null);
                        final ImageView image = (ImageView) imagen.findViewById(R.id.image);
                        Bitmap b = BitmapFactory.decodeResource(Aplication.context.getResources(), R.drawable.generic_image_rectangular);
                        float proportion = (float) b.getWidth() / b.getHeight();
                        int alto = (int) ((Aplication.screenW) / proportion);
                        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
                        lp.height = alto;
                        lp.width = Aplication.screenW;
                        image.setLayoutParams(lp);
                        ImageLoader.getInstance().displayImage(map.get("med"), image, new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String s, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String s, View view, FailReason failReason) {

                            }

                            @Override
                            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                                float proportion = (float) bitmap.getWidth() / bitmap.getHeight();
                                int alto = (int) (Aplication.screenW / proportion);
                                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
                                lp.height = alto;
                                lp.width = Utils.getScreenWidth(getActivity());
                                image.setLayoutParams(lp);
                            }

                            @Override
                            public void onLoadingCancelled(String s, View view) {

                            }
                        });
                        container.addView(imagen);
                        continue;
                    }
                    if (map.size() == 4) {//es un video
                        final View video = inflater.inflate(R.layout.news_video, null);
                        final ImageView video_image = ((ImageView) video.findViewById(R.id.video_image));
                        Bitmap b = BitmapFactory.decodeResource(Aplication.context.getResources(), R.drawable.generic_image_rectangular);
                        float proportion = (float) b.getWidth() / b.getHeight();
                        int alto = (int) ((Aplication.screenW) / proportion);
                        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) video_image.getLayoutParams();
                        lp.height = alto;
                        lp.width = Aplication.screenW;
                        video_image.setLayoutParams(lp);
                        final String video_url = map.get("video");
                        ImageLoader.getInstance().displayImage(map.get("med"), video_image, new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String s, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String s, View view, FailReason failReason) {

                            }

                            @Override
                            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                                float proportion = (float) bitmap.getWidth() / bitmap.getHeight();
                                int alto = (int) (Utils.getScreenWidth(getActivity()) / proportion);
                                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) video_image.getLayoutParams();
                                lp.height = alto;
                                lp.width = Utils.getScreenWidth(getActivity());
                                video_image.setLayoutParams(lp);
                            }

                            @Override
                            public void onLoadingCancelled(String s, View view) {

                            }
                        });
                        video.findViewById(R.id.play).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!video_url.startsWith("https://www.youtube.com/watch")) {
                                    Intent video = new Intent(getActivity(), VideoActivity.class);
                                    video.putExtra("uri", video_url);
                                    startActivity(video);
                                } else {
                                    Utils.playVideoYoutube(getActivity(), video_url);
                                }
                            }
                        });
                        container.addView(video);
                    }
                }
            }
            if (item instanceof ArrayList) {//slide
                View galeria = inflater.inflate(R.layout.news_gallery, null);
                ViewPager pager = (ViewPager) galeria.findViewById(R.id.pager);
                GalleryAdapter galleryAdapter = new GalleryAdapter(pager, (ArrayList<LinkedTreeMap<String, String>>) item);
                pager.setAdapter(galleryAdapter);
                container.addView(galeria);
            }
        }

        if(media.size()>1){
            for (int i=1; i<media.size();i++){
                View item_media= ((LayoutInflater) Aplication.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.news_media_container, null);
                LinkedTreeMap<String,String> data= (LinkedTreeMap<String, String>) media.get(i);
                if(data.size()!=2)
                    continue;
                ((TextView)item_media.findViewById(R.id.pdf_name)).setText(data.get("title"));
                ((TextView)item_media.findViewById(R.id.pdf_name)).setTypeface(Aplication.hregular);
                //ponemos la url en el tag del root
                item_media.setTag(data.get("file"));
                item_media.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent reader= new Intent(getActivity(), ReaderActivity.class);
                        reader.putExtra("url",(String)v.getTag());
                        getActivity().startActivity(reader);
                    }
                });
                container.addView(item_media);
            }

        }
        return layout;
    }
   
    @Override
    public void onDetach() {
        super.onDetach();
    }

    class GalleryAdapter extends PagerAdapter {


        @Bind(R.id.image)
        ImageView image;
        @Bind(R.id.next)
        View next;
        @Bind(R.id.prev)
        View prev;
        ViewPager pager;
        boolean isViewpagerResized = false;

        ArrayList<LinkedTreeMap<String, String>> items;


        public GalleryAdapter(ViewPager pager, ArrayList<LinkedTreeMap<String, String>> items) {
            this.pager = pager;
            this.items = items;
            Bitmap b= BitmapFactory.decodeResource(Aplication.context.getResources(),R.drawable.generic_image_rectangular);
            float proportion = (float) b.getWidth() / b.getHeight();
            int alto = (int) ((Aplication.screenW) / proportion);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) pager.getLayoutParams();
            lp.height = alto;
            lp.width = Aplication.screenW;
            pager.setLayoutParams(lp);
        }

        @Override
        public Object instantiateItem(final ViewGroup container, int position) {
            final LinkedTreeMap<String, String> galleryItem = items.get(position);
            View view = ((LayoutInflater) inflater).inflate(R.layout.news_gallery_item, container, false);
            ButterKnife.bind(this, view);
            ImageLoader.getInstance().displayImage(Uri.parse(galleryItem.get("med")).toString(), image, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {

                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    float proportion = (float) bitmap.getWidth() / bitmap.getHeight();
                    int alto = (int) (Utils.getScreenWidth(getActivity()) / proportion);
                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
                    lp.height = alto;
                    lp.width = Utils.getScreenWidth(getActivity());
                    image.setLayoutParams(lp);
                    if (!isViewpagerResized) {
                        pager.setLayoutParams(lp);
                        isViewpagerResized = true;
                    }
                }

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });

            if (position < items.size() - 1) {
                next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (pager.getCurrentItem() + 1 <= items.size() - 1)
                            pager.setCurrentItem(pager.getCurrentItem() + 1);
                    }
                });
            } else
                next.setVisibility(View.GONE);
            if (position > 0) {
                prev.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (pager.getCurrentItem() - 1 >= 0)
                            pager.setCurrentItem(pager.getCurrentItem() - 1);
                    }
                });
            } else
                prev.setVisibility(View.GONE);
            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void destroyItem(View container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }
}
