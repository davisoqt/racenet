package com.muevaelvolante.racenet.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.model.notification.Notification;
import com.muevaelvolante.racenet.model.notification.PushNotification;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NotificationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NotificationFragment extends RootFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private LayoutInflater inflater;


    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public void proccessData(Object data) {
        Notification noty= new Gson().fromJson((String)data,Notification.class);
        SharedPreferences preferences= getActivity().getSharedPreferences("chanels",Context.MODE_PRIVATE);
        if(noty.getPushNotificationsIntroText() != null && noty.getPushNotificationsIntroText().size()>0)
        text.setText(Html.fromHtml(noty.getPushNotificationsIntroText().get(0).getText()));
        for (PushNotification push_noty :
                noty.getPushNotifications()) {
            View item_row = inflater.inflate(R.layout.notification_row, container, false);
            item_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getActivity(),"Activar o desactivar el registro",Toast.LENGTH_SHORT).show();
                }
            });
            ((TextView)item_row.findViewById(R.id.noti_name)).setTypeface(Aplication.hmedium);
            ((TextView)item_row.findViewById(R.id.noti_name)).setText(push_noty.getName());
            CheckBox checkBox= (CheckBox)item_row.findViewById(R.id.checkBox);
            container.addView(item_row);
            /**
             * Guardamos en preferences los ARns
             * (arn, int)
             * -1 no existe, 0 no esta registrado,1 esta registrado
             */
            int status= preferences.getInt(push_noty.getARN(),-1);
            if(status==-1)
                preferences.edit().putInt(push_noty.getARN(),0).commit();
            if(status==-1 || status==0)
                checkBox.setChecked(false);
            else
                checkBox.setChecked(true);
        }
        hideDialog();
    }

    public static NotificationFragment newInstance(String param1, String param2) {
        NotificationFragment fragment = new NotificationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString("t", param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uri = getArguments().getString(ARG_PARAM1);
            json_name = "noty";
            title_name = getArguments().getString("t");
        }
    }

    @Bind(R.id.container)
    LinearLayout container;
    @Bind(R.id.text)
    TextView text;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout=inflater.inflate(R.layout.fragment_notification, container, false);
        this.inflater= inflater;
        ButterKnife.bind(this,layout);
        text.setTypeface(Aplication.hregular);
        return layout;
    }

}
