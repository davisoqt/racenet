package com.muevaelvolante.racenet.fragment;


import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.HomeActivity;
import com.muevaelvolante.racenet.cache.JsonCacheReader;
import com.muevaelvolante.racenet.cache.JsonCacheWriter;
import com.muevaelvolante.racenet.commons.Constants;
import com.muevaelvolante.racenet.interfaces.IData;
import com.muevaelvolante.racenet.model.setting.Setting;
import com.muevaelvolante.racenet.service.ServiceGenerator;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class RootFragment extends Fragment {

    private MaterialDialog mProgressDialog;
    public String uri;
    public String json_name;
    public String title_name;

    public RootFragment() {
        // Required empty public constructor
    }

    public void showProgressDialog(String text) {
        mProgressDialog =new MaterialDialog.Builder(getActivity())
                .content("Loading...")
                .progress(true, 0)
                .widgetColor(Color.parseColor("#454553"))
                .cancelable(false)
                .build();
        mProgressDialog.show();
    }

    public void hideDialog(){
        mProgressDialog.dismiss();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new LoadData().execute(new String[]{uri});
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity)getActivity()).setToolBarTitle(title_name);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    public abstract void proccessData(Object data);

    class LoadData extends AsyncTask<String,Void,Object>{

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            proccessData(o);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog("");
        }

        @Override
        protected Object doInBackground(String... strings) {
            IData iData= ServiceGenerator.createService(IData.class);
            Call<ResponseBody> result= iData.getData(strings[0]);
            String data;
            try {
                data = result.execute().body().string();
                JSONObject jsonObject= new JSONObject(data);
                JsonCacheWriter.writeCache(Constants.jsonCacheRootDir + "/"+json_name+".json", (data));
                return data;
            } catch (Exception e) {
                e.printStackTrace();
                String jsonOfflineData = JsonCacheReader.getCacheJson(Constants.jsonCacheRootDir + "/"+json_name+".json");
                if (jsonOfflineData != null) {
                    return jsonOfflineData;
                }else
                    return null;
            }
        }
    }
}
