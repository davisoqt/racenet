package com.muevaelvolante.racenet.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.HomeActivity;
import com.muevaelvolante.racenet.adapters.RouteAdapter;
import com.muevaelvolante.racenet.model.route.Route;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RuoteFragment extends RootFragment {
    private static final String ARG_PARAM1 = "param1";
    @Bind(R.id.list)
    RecyclerView recyclerView;
    private Menu menu;

    public RuoteFragment() {
        // Required empty public constructor
    }

    @Override
    public void proccessData(Object data) {
        hideDialog();
        recyclerView.setAdapter(new RouteAdapter(new Gson().fromJson((String) data,Route.class), getActivity()));
    }
    // TODO: Rename and change types and number of parameters
    public static RuoteFragment newInstance(String param1, String param2) {
        RuoteFragment fragment = new RuoteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString("t",param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uri = getArguments().getString(ARG_PARAM1);
            json_name="route";
            title_name=getArguments().getString("t");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout= inflater.inflate(R.layout.fragment_route, container, false);
        ButterKnife.bind(this,layout);
        return layout;
    }

    @Override
    public void onDetach() {
        super.onDetach();
       // menu.findItem(0).setVisible(true);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((HomeActivity)context).hideSearch();
    }
}
