package com.muevaelvolante.racenet.fragment;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.adapters.ScheduleAdapter;
import com.muevaelvolante.racenet.adapters.ScheduleFilterAdapter;
import com.muevaelvolante.racenet.model.agenda.Agenda;
import com.muevaelvolante.racenet.model.agenda.Day;
import com.muevaelvolante.racenet.util.Utils;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ScheduleFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    @Bind(R.id.list)
    RecyclerView recyclerView;
    @Bind(R.id.list2)
            RecyclerView filterRV;
    Agenda agenda;
    @Bind(R.id.textView6)
    TextView day1;
    @Bind(R.id.textView4)
    TextView day2;
    @Bind(R.id.textView66)
    TextView day3;
    @Bind(R.id.textView17)
    TextView filter;
    @Bind(R.id.day1)
    View day1_container;
    @Bind(R.id.day2)
    View day2_container;
    @Bind(R.id.day3)
    View day3_container;
    @Bind(R.id.date_string)
    TextView date_string;
    @Bind(R.id.query)
    TextView query;
    String selected_color="#454553";
    String unselected_color="#b5b5ba";
    private ScheduleAdapter scheduleAdapter;

    public TextView getQuery() {
        return query;
    }

    public void setQuery(TextView query) {
        this.query = query;
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public ScheduleAdapter getScheduleAdapter() {
        return scheduleAdapter;
    }

    public Agenda getAgenda() {
        return agenda;
    }

    public ScheduleFragment() {
        // Required empty public constructor
    }

    public static ScheduleFragment newInstance(Agenda data) {
        ScheduleFragment fragment = new ScheduleFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, data);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            agenda = (Agenda) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout= inflater.inflate(R.layout.fragment_schedule, container, false);
        ButterKnife.bind(this,layout);
        scheduleAdapter=new ScheduleAdapter(0,getActivity(),agenda);
        recyclerView.setAdapter(scheduleAdapter);
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getContext()).color(Color.parseColor("#d5d5db"))
                .build());
        filterRV.setAdapter(new ScheduleFilterAdapter(this,agenda.getFilters()));
        filterRV.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getContext()).color(Color.parseColor("#ffffff"))
                .build());
        day2.setTypeface(Aplication.hregular);
        day1.setTypeface(Aplication.hregular);
        filter.setTypeface(Aplication.hregular);
        date_string.setTypeface(Aplication.hlight);
        query.setTypeface(Aplication.hmedium);
        DateTime d= Utils.convertStringToDate2(agenda.getDay1().get(0).getDate());
        LocalDate l= d.toLocalDate();
        date_string.setText(l.dayOfWeek().getAsText(Locale.CANADA)+", "+l.monthOfYear().getAsText(Locale.CANADA)+" "+l.getDayOfMonth());
        if(agenda.getDay1()==null || agenda.getDay1().size()==0 )
            day1_container.setVisibility(View.GONE);
        if(agenda.getDay2()==null || agenda.getDay2().size()==0 )
            day2_container.setVisibility(View.GONE);
        if(agenda.getDay3()==null || agenda.getDay3().size()==0 )
            day3_container.setVisibility(View.GONE);
        return layout;
    }

    @OnClick(R.id.day2)
    public void day2Click(){
        scheduleAdapter.resetProcesed();
        scheduleAdapter.setViewType(1);
        scheduleAdapter.setSerachTarget(1);
        scheduleAdapter.notifyDataSetChanged();
        query.setText("");
        day1_container.setBackgroundResource(R.drawable.button_unselected_agenda);
        day1.setTextColor(Color.parseColor(unselected_color));
        day2_container.setBackgroundResource(R.drawable.button_selected_agenda);
        day2.setTextColor(Color.parseColor(selected_color));
        day3_container.setBackgroundResource(R.drawable.button_unselected_agenda);
        day3.setTextColor(Color.parseColor(unselected_color));
        DateTime d= Utils.convertStringToDate2(agenda.getDay2().get(0).getDate());
        LocalDate l= d.toLocalDate();
        date_string.setText(l.dayOfWeek().getAsText(Locale.CANADA)+", "+l.monthOfYear().getAsText(Locale.CANADA)+" "+l.getDayOfMonth());
    }

    @OnClick(R.id.day1)
    public void day1Click(){
        scheduleAdapter.resetProcesed();
        scheduleAdapter.setViewType(0);
        scheduleAdapter.setSerachTarget(0);
        scheduleAdapter.notifyDataSetChanged();
        query.setText("");
        day1_container.setBackgroundResource(R.drawable.button_selected_agenda);
        day1.setTextColor(Color.parseColor(selected_color));
        day2_container.setBackgroundResource(R.drawable.button_unselected_agenda);
        day2.setTextColor(Color.parseColor(unselected_color));
        day3_container.setBackgroundResource(R.drawable.button_unselected_agenda);
        day3.setTextColor(Color.parseColor(unselected_color));
        DateTime d= Utils.convertStringToDate2(agenda.getDay1().get(0).getDate());
        LocalDate l= d.toLocalDate();
        date_string.setText(l.dayOfWeek().getAsText(Locale.CANADA)+", "+l.monthOfYear().getAsText(Locale.CANADA)+" "+l.getDayOfMonth());
    }

    @OnClick(R.id.day3)
    public void day3Click(){
        scheduleAdapter.resetProcesed();
        scheduleAdapter.setViewType(3);
        scheduleAdapter.setSerachTarget(3);
        scheduleAdapter.notifyDataSetChanged();
        query.setText("");
        day3_container.setBackgroundResource(R.drawable.button_selected_agenda);
        day3.setTextColor(Color.parseColor(selected_color));
        day2_container.setBackgroundResource(R.drawable.button_unselected_agenda);
        day2.setTextColor(Color.parseColor(unselected_color));
        day1_container.setBackgroundResource(R.drawable.button_unselected_agenda);
        day1.setTextColor(Color.parseColor(unselected_color));
        DateTime d= Utils.convertStringToDate2(agenda.getDay3().get(0).getDate());
        LocalDate l= d.toLocalDate();
        date_string.setText(l.dayOfWeek().getAsText(Locale.CANADA)+", "+l.monthOfYear().getAsText(Locale.CANADA)+" "+l.getDayOfMonth());
    }
    @OnClick(R.id.textView17)
    public void filterClick(){
        showFilter();
    }

    public void showFilter() {
        if (filterRV.getVisibility() == View.VISIBLE) {
            filterRV.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.slide_top_out));
            filterRV.setVisibility(View.GONE);
        } else {
            filterRV.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.slide_top_in));
            filterRV.setVisibility(View.VISIBLE);
        }
    }
}
