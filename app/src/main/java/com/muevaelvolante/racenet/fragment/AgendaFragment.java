package com.muevaelvolante.racenet.fragment;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.vision.text.Text;
import com.google.gson.Gson;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.EventMapActivity;
import com.muevaelvolante.racenet.activity.HomeActivity;
import com.muevaelvolante.racenet.model.agenda.Agenda;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AgendaFragment extends RootFragment {
    private static final String ARG_PARAM1 = "param1";

    private String mParam1;
    private String mParam2;
    private ViewPagerAdapter adapter;

    public AgendaFragment() {
        // Required empty public constructor
    }
    String selected_tabColor = "#ffffff";
    String unSelected_tabColor = "#64c5c5c8";

    @Bind(R.id.imageView14)
    ImageView image;
    @Bind(R.id.textView15)
    TextView short_text;
    @Bind(R.id.textView16)
    TextView text;
    @Bind(R.id.title)
    TextView title;
    @Bind(R.id.date)
    TextView date;
    @Bind(R.id.text_container)
    LinearLayout text_container;
    @Bind(R.id.imageView15)
    View shadow;
    @Bind(R.id.viewpager)
    ViewPager pager;
    @Bind(R.id.btn_map)
    View event_map;
    @Bind(R.id.imageView5)
    View sch_indicator;
    @Bind(R.id.textView10)
    TextView sch_text;
    @Bind(R.id.imageView6)
    View spk_indicator;
    @Bind(R.id.textView9)
    TextView spk_text;
    @Bind(R.id.tab_container)
            View tab_view;

    Agenda agenda;

    @Override
    public void proccessData(Object data) {
        agenda = new Gson().fromJson((String) data, Agenda.class);
        //cargamos el home
        String homeUrl;
        if (getContext().getResources().getBoolean(R.bool.is_tablet))
            homeUrl = agenda.getHome().get(0).getMedia().get(0).getHi();
        else
            homeUrl = agenda.getHome().get(0).getMedia().get(0).getMed();
        ImageLoader.getInstance().displayImage(homeUrl, image, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {

            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                float proportion = (float) bitmap.getWidth() / bitmap.getHeight();
                int alto = (int) (Aplication.screenW / proportion);
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
                lp.height = alto;
                lp.width = Aplication.screenW;
                image.setLayoutParams(lp);
                shadow.setLayoutParams(lp);
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });
        short_text.setText(agenda.getHome().get(0).getQuote());
        title.setText(agenda.getHome().get(0).getTitle());
        date.setText(agenda.getHome().get(0).getDates());
        text.setText(agenda.getHome().get(0).getShortText());
        adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(ScheduleFragment.newInstance(agenda), "ONE");
        adapter.addFragment(SpeakersFragment.newInstance(agenda.getSpeakers(),""), "TWO");
        adapter.addFragment(new EmptyFragment(), "THREE");
        pager.setAdapter(adapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position==2){
                   pager.setCurrentItem(1);
                    Intent map= new Intent(getActivity(), EventMapActivity.class);
                    Bundle data= new Bundle();
                    data.putSerializable("d",agenda.getCityMap());
                    data.putSerializable("dd",agenda.getVenueMap().get(0));
                    map.putExtras(data);
                    getActivity().startActivity(map);
                    return;
                }
                if(position==0){
                    spk_indicator.setVisibility(View.INVISIBLE);
                    spk_text.setTextColor(Color.parseColor(unSelected_tabColor));
                    sch_indicator.setVisibility(View.VISIBLE);
                    sch_text.setTextColor(Color.parseColor(selected_tabColor));
                }else if(position==1){
                    spk_indicator.setVisibility(View.VISIBLE);
                    spk_text.setTextColor(Color.parseColor(selected_tabColor));
                    sch_indicator.setVisibility(View.INVISIBLE);
                    sch_text.setTextColor(Color.parseColor(unSelected_tabColor));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        hideDialog();
    }

    @OnClick({R.id.btn_schedule, R.id.btn_speaker})
    public void showTabs(View view){
        Animation anim= AnimationUtils.loadAnimation(getActivity(),R.anim.slide_in_forward);
        if(view.getId()==R.id.btn_speaker)
        pager.setCurrentItem(1);
        tab_view.setAnimation(anim);
        tab_view.setVisibility(View.VISIBLE);
    }

    public static AgendaFragment newInstance(String uri, String param2) {
        AgendaFragment fragment = new AgendaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, uri);
        args.putString("t", param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uri = getArguments().getString(ARG_PARAM1);
            json_name = "agenda";
            title_name = getArguments().getString("t");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((HomeActivity)context).hideSearch();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_agenda, container, false);
        ButterKnife.bind(this, layout);
        Bitmap b = BitmapFactory.decodeResource(Aplication.context.getResources(), R.drawable.generic_image_rectangular);
        float proportion = (float) b.getWidth() / b.getHeight();
        int alto = (int) ((Aplication.screenW) / proportion);
        ViewGroup.LayoutParams lp = (ViewGroup.LayoutParams) image.getLayoutParams();
        lp.height = alto;
        lp.width = Aplication.screenW;
        image.setLayoutParams(lp);
        shadow.setLayoutParams(lp);
        date.setTypeface(Aplication.hregular);
        title.setTypeface(Aplication.hlight);
        short_text.setTypeface(Aplication.hmedium);
        // ((FrameLayout)image.getParent()).setLayoutParams(lp);
        adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        return layout;
    }

    @OnClick(R.id.btn_map)
    public void mapClick(){
        Intent map= new Intent(getActivity(), EventMapActivity.class);
        Bundle data= new Bundle();
        data.putSerializable("d",agenda.getCityMap());
        data.putSerializable("dd",agenda.getVenueMap().get(0));
        map.putExtras(data);
        getActivity().startActivity(map);
    }

    @OnClick(R.id.textView91)
    public void mapClick2(){
        Intent map= new Intent(getActivity(), EventMapActivity.class);
        Bundle data= new Bundle();
        data.putSerializable("d",agenda.getCityMap());
        data.putSerializable("dd",agenda.getVenueMap().get(0));
        map.putExtras(data);
        getActivity().startActivity(map);
    }

    @OnClick(R.id.root_indicator2)
    public void speakerClick(){
        pager.setCurrentItem(1);
    }

    @OnClick(R.id.root_indicator1)
    public void scheduleClick(){
        pager.setCurrentItem(0);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
          //  manager.getFragments().clear();
            pager.setOffscreenPageLimit(0);
        }

        @Override
        public Fragment getItem(int position) {
               return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
