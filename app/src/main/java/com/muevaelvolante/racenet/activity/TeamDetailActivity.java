package com.muevaelvolante.racenet.activity;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.fragment.TeamDetailFragment;
import com.muevaelvolante.racenet.model.team.Team_;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TeamDetailActivity extends AppCompatActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toobar_title;
    @Bind(R.id.pager)
    ViewPager pager;

    private TeamAdapter mTeamAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_detail);
        ButterKnife.bind(this);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_back);
        getSupportActionBar().setHomeButtonEnabled(true);
        toobar_title.setTypeface(Aplication.hmedium);
        initViewPager(getIntent().getExtras().getInt("pos"));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()== android.R.id.home)
            finish();
        return true;
    }


    /**
     * Método para la inicialización del ViewPager.
     *
     * @param position
     */
    private void initViewPager(int position) {

        this.mTeamAdapter = new TeamAdapter(getSupportFragmentManager(), Aplication.teamList);

        pager.setAdapter(mTeamAdapter);
        pager.setCurrentItem(position);

    }



    public class TeamAdapter extends FragmentPagerAdapter {

        /*
         * Objeto usuario.
         */
        List<Team_> mFeeds;


        /**
         * Constructor por defecto.
         *
         * @param fm
         * @param feeds
         */
        public TeamAdapter(android.support.v4.app.FragmentManager fm, List feeds) {

            super(fm);
            this.mFeeds = feeds;

        }


        @Override
        public android.support.v4.app.Fragment getItem(int pos) {

			/*
			 * Todas las pantallas son idénticas, siendo asociada cada una con un objeto Feed.
			 */
            android.support.v4.app.Fragment fragment = TeamDetailFragment.newInstance(this.mFeeds.get(pos));

            return fragment;

        }


        @Override
        public int getCount() {

            return this.mFeeds.size();

        }
    }
}
