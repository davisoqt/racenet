package com.muevaelvolante.racenet.activity;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.gson.Gson;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.adapters.HomeAdapter;
import com.muevaelvolante.racenet.adapters.HomeSearchAdapter;
import com.muevaelvolante.racenet.cache.JsonCacheReader;
import com.muevaelvolante.racenet.cache.JsonCacheWriter;
import com.muevaelvolante.racenet.commons.Constants;
import com.muevaelvolante.racenet.fragment.AgendaFragment;
import com.muevaelvolante.racenet.fragment.FragmentMenu;
import com.muevaelvolante.racenet.fragment.PartnerFragment;
import com.muevaelvolante.racenet.fragment.RaceScheduleFragment;
import com.muevaelvolante.racenet.fragment.RuoteFragment;
import com.muevaelvolante.racenet.fragment.TeamFragment;
import com.muevaelvolante.racenet.fragment.WhoIsWhoFragment;
import com.muevaelvolante.racenet.interfaces.IPost;
import com.muevaelvolante.racenet.interfaces.ISettings;
import com.muevaelvolante.racenet.model.notification.Notification;
import com.muevaelvolante.racenet.model.notification.PushNotification;
import com.muevaelvolante.racenet.model.post.Post;
import com.muevaelvolante.racenet.model.search.Search;
import com.muevaelvolante.racenet.model.setting.Setting;
import com.muevaelvolante.racenet.push.SNSRegisterTask;
import com.muevaelvolante.racenet.service.ServiceGenerator;
import com.muevaelvolante.racenet.util.ExceptionHandler;
import com.muevaelvolante.racenet.util.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;

@SuppressWarnings("WrongConstant")
public class HomeActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, FragmentMenu.MenuListener,GoogleApiClient.OnConnectionFailedListener {

    //matenemos en memoria el fragmentMenu
    FragmentMenu menuFragment;
    Fragment currentFragment;
    private MaterialDialog mProgressDialog;
    private boolean isFilterOpen=false;
    private HomeAdapter homeAdapter;
    @Bind(R.id.editText)
    TextView filter_textl;

    public Setting getSettings() {
        return settings;
    }

    private Setting settings;
    boolean isMenuOpen;
    @Bind(R.id.toolbar_title)
    TextView toobar_title;
    @Bind(R.id.list)
    RecyclerView recyclerView;
    @Bind(R.id.imageView10)
    View spinner;
    @Bind(R.id.swiperefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.list2)
    RecyclerView filter;
    private Menu menu;
    @Bind(R.id.filter_container)
    View filter_container;
    private GoogleApiClient mGoogleApiClient;
    PushReceiver pushReceiver;
    MaterialDialog pushDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        pushReceiver= new PushReceiver();
        Aplication.screenW=(Utils.getScreenWidth(this));
        ButterKnife.bind(this);
        ExceptionHandler.register(this,"Error volvo","leoperezortiz@gmail.com");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setNavigationIcon(R.drawable.ic_action_menu);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (menuFragment == null)
                    menuFragment = FragmentMenu.newInstance(0, settings.getMenu());
                getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_forward, R.anim.slide_out_forward).replace(R.id.menu_container, menuFragment).commit();
                isMenuOpen = true;
            }
        });
        toobar_title.setTypeface(Aplication.hmedium);
        toobar_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentFragment instanceof ToolBarTitleAction)
                    ((ToolBarTitleAction)currentFragment).toolBarClick();
            }
        });
        filter_textl.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager obj = (InputMethodManager)HomeActivity.this.getSystemService("input_method");
                    if (obj != null)
                    {
                        ((InputMethodManager) (obj)).hideSoftInputFromWindow(filter_textl.getWindowToken(), 0);
                    }
                    new SearchTask(ServiceGenerator.BASE_API_SEARCH_TEXT_URL+textView.getText().toString()).execute();
                }
                return false;
            }
        });
        loadALl();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    public void setToolBarTitle(String title){
        toobar_title.setText(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        this.menu= menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(currentFragment!=null)
        currentFragment.onOptionsItemSelected(item);
        if(item.getItemId()== R.id.action_search)
            if(currentFragment instanceof ToolBarTitleAction)
                ((ToolBarTitleAction) currentFragment).toolBarClick();
            else
            showFilter();
        return true;
    }

    public void hideSearch(){
        menu.getItem(0).setVisible(false);
    }

    public void showSearch(){
        menu.getItem(0).setVisible(true);
    }

    void loadALl() {
        new Loadall().execute();
    }

    @Override
    public void onBackPressed() {
        if (isMenuOpen)
            closeMenu();
    }

    @Override
    public void closeMenu() {
        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_forward, R.anim.slide_out_forward).remove(menuFragment).commit();
        isMenuOpen = false;
    }

    public void spinnerOn(){
        spinner.setVisibility(View.VISIBLE);
    }

    public void spinnerOff(){
        spinner.setVisibility(View.GONE);
    }

    public void showProgressDialog(String text) {
        mProgressDialog =new MaterialDialog.Builder(this)
                .progress(true, 0)
                .widgetColor(Color.parseColor("#454553"))
                .cancelable(false)
                .build();
        mProgressDialog.setContent(text);
        mProgressDialog.show();
    }

    private void showErrorDialog(String text) {
        mProgressDialog = new MaterialDialog.Builder(this)
                .title("Result")
                .content(text)
                .widgetColor(Color.parseColor("#454553"))
                .positiveColor(Color.parseColor("#454553"))
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        mProgressDialog.show();
    }

    void showFilter(){
        if(!isFilterOpen) {
            isFilterOpen=true;
            filter_container.setAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_top_in));
            filter_container.setVisibility(View.VISIBLE);
        }else
        {
            filter_container.setAnimation(AnimationUtils.loadAnimation(this,R.anim.slide_top_out));
            filter_container.setVisibility(View.GONE);
            isFilterOpen=false;
        }
    }

    public void search(String url){
        new SearchTask(url).execute();
    }

    @Override
    public void gotoSection(Fragment section) {
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .showImageOnLoading(R.drawable.generic_image_rectangular)
                .showImageOnFail(R.drawable.generic_image_rectangular)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                //.memoryCacheExtraOptions(480,480)
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().destroy();
        ImageLoader.getInstance().init(config);
        if(isMenuOpen)
            closeMenu();
        if(currentFragment==null && section==null)
            return;
        if(section==null) {
            getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_forward, R.anim.slide_out_forward).remove(currentFragment).commit();
            toobar_title.setText("RaceNet");
            showSearch();
        }
        else
         getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_forward,R.anim.slide_out_forward).replace(R.id.section_container,section).commit();
            currentFragment=section;
    }

    @Override
    public void logOut() {
        if(mGoogleApiClient.isConnected()){
            Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            // ...
                        }
                    });
        }
        getSharedPreferences("login",MODE_PRIVATE).edit().putBoolean("isLoged",false).commit();
        startActivity(new Intent(this,SplashActivity.class));
        ImageLoader.getInstance().clearMemoryCache();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(pushReceiver,new IntentFilter("PUSH_ADDED"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(pushReceiver);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        loadALl();
    }

    private void openSection(String target) {
        com.muevaelvolante.racenet.model.setting.Menu menu= settings.getMenuItemPosition(target);
        if(menu.getID().toLowerCase().equals("teams")){
            gotoSection(TeamFragment.newInstance(menu.getURL(), "Teams"));
            return;
        }
        if(menu.getID().toLowerCase().equals("route")){
            gotoSection(RuoteFragment.newInstance(menu.getURL(),"Route"));
            return;
        }
        if(menu.getID().toLowerCase().equals("partners")){
            gotoSection(PartnerFragment.newInstance(menu.getURL(),"Partners"));
            return;
        }
        if(menu.getID().toLowerCase().equals("whoiswho")){
            gotoSection(WhoIsWhoFragment.newInstance(menu.getURL(),"Who is Who"));
            return;
        }
        if(menu.getID().toLowerCase().equals("conference-agenda")){
            gotoSection(AgendaFragment.newInstance(menu.getURL(),"Conference Agenda"));
            return;
        }
        if(menu.getID().equals("SCHEDULE")){
            gotoSection(RaceScheduleFragment.newInstance(menu.getURL(),"Race Schedule"));
            return;
        }
        if(menu.getID().toLowerCase().equals("logout")){
            logOut();
            return;
        }
        if(menu.getID().toLowerCase().equals("home")){
            gotoSection(null);
            return;
        }
    }

    public void loadNoFilterData() {
        new Loadall().execute();
        showFilter();
    }

    class Loadall extends AsyncTask<Void, Void, ArrayList<Object>> {

        @Override
        protected void onPostExecute(ArrayList<Object> objs) {
            super.onPostExecute(objs);
            if(objs!= null){
                settings = (Setting) objs.get(0);
                menuFragment = FragmentMenu.newInstance(0, settings.getMenu());
                homeAdapter=new HomeAdapter((Post) objs.get(1), HomeActivity.this,true);
                recyclerView.setAdapter(homeAdapter);
                if(getIntent().getExtras()!=null){//si llega alguna seccion o action a ejecutar
                    Bundle data= getIntent().getExtras();
                    if(data.getString("action")!=null && data.getString("action").toLowerCase().equals("go")){
                        openSection(data.getString("target"));
                    }else if(data.getString("action")!=null && data.getString("action").toLowerCase().equals("url")){
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(data.getString("target"))));
                    }
                }
                //registro la notificacion por defecto si esta no existe en las preferencias aun
                for (PushNotification notyy :
                        settings.getPushNotifications()) {
                    if(notyy.getDefault().equals("1"))
                    {
                        new SNSRegisterTask(getApplicationContext(),new String[]{notyy.getARN()}).execute();
                    }
                    getSharedPreferences("chanels",MODE_PRIVATE).edit().putInt(notyy.getARN(),0);
                }
            }
            filter.setAdapter(new HomeSearchAdapter(((Post) objs.get(1)).getCategories(),null,HomeActivity.this));
            if(swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
            else
                mProgressDialog.dismiss();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(!swipeRefreshLayout.isRefreshing())
                showProgressDialog("Loading...");
        }

        @Override
        protected ArrayList<Object> doInBackground(Void... voids) {
            Setting setting;
            Post post;
            ArrayList<Object> list = new ArrayList<>();
            try {
                ISettings ISettings = ServiceGenerator.createService(ISettings.class);
                Call<Setting> call = ISettings.getData(ServiceGenerator.BASE_URL + "settings.json");
                setting = call.execute().body();
                Gson gson = new Gson();
                JsonCacheWriter.writeCache(Constants.jsonCacheRootDir + "/settings.json", gson.toJson(setting));

            } catch (IOException ex) {
                Gson gson = new Gson();
                String jsonOfflineData = JsonCacheReader.getCacheJson(Constants.jsonCacheRootDir + "/settings.json");
                if (jsonOfflineData != null) {
                    setting = gson.fromJson(jsonOfflineData, Setting.class);
                }else
                    return null;
            }
            list.add(setting);
            try {
                IPost iPost = ServiceGenerator.createService(IPost.class);
                Call<Post> call2 = iPost.getPosts(setting.getMenu().get(0).getURL());
                post = call2.execute().body();
                list.add(post);
                Gson gson = new Gson();
                JsonCacheWriter.writeCache(Constants.jsonCacheRootDir + "/post.json", gson.toJson(post));
                return list;
            } catch (IOException ex) {
                Gson gson = new Gson();
                String jsonOfflineData = JsonCacheReader.getCacheJson(Constants.jsonCacheRootDir + "/post.json");
                if (jsonOfflineData != null) {
                    post = gson.fromJson(jsonOfflineData, Post.class);
                    list.add(post);
                    return list;
                }
                else
                    return null;
            }
        }
    }

    public class SearchTask extends AsyncTask<Void,Void,Search>{


        String url;

        public SearchTask(String url) {
            this.url = url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog("Searching...");
        }

        @Override
        protected Search doInBackground(Void... params) {
            IPost iPost= ServiceGenerator.createService(IPost.class);
            Call<Search> searchCal= iPost.search(url);
            try {
                Search result=searchCal.execute().body();
                return result;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Search search) {
            super.onPostExecute(search);
            mProgressDialog.dismiss();
            if( search== null){
                showErrorDialog("Empty");
                showFilter();
            }else {
                homeAdapter.setConferenceNews(search.getConferenceNews());
                homeAdapter.setReaceNew(search.getRacenetNews());
                homeAdapter.notifyDataSetChanged();
                showFilter();
            }
        }
    }

    public interface ToolBarTitleAction{
        void toolBarClick();
    }

    public class PushReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            String mfs= bundle.getString("message");
            try {
                JSONObject object= new JSONObject(mfs);
                String title= object.getString("title");
                String message= object.getString("msg");
                final String action= object.getString("action");
                final String target= object.getString("target");
                Log.d("json", object.getString("msg"));
                if (pushDialog !=null && pushDialog.isShowing())
                    return;
                pushDialog= new MaterialDialog.Builder(HomeActivity.this)
                       .title(title)
                       .content(message)
                       .widgetColor(Color.parseColor("#454553"))
                       .positiveColor(Color.parseColor("#454553"))
                       .positiveText("Ok")
                       .onPositive(new MaterialDialog.SingleButtonCallback() {
                           @Override
                           public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                               pushDialog.dismiss();
                               if(action!=null && action.toLowerCase().equals("go")){
                                   openSection(target);
                               }else if(action!=null && action.toLowerCase().equals("url")){
                                   startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(target)));
                               }
                           }
                       })
                       .negativeColor(Color.parseColor("#454553"))
                       .negativeText("Cancel")
                       .onNegative(new MaterialDialog.SingleButtonCallback() {
                           @Override
                           public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                               pushDialog.dismiss();
                           }
                       })
                       .build();
                pushDialog.show();
                NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.cancel(23);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
