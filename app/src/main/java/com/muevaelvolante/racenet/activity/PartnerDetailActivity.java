package com.muevaelvolante.racenet.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.internal.LinkedTreeMap;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.fragment.PartnerDetailFragment;
import com.muevaelvolante.racenet.fragment.TeamDetailFragment;
import com.muevaelvolante.racenet.model.partner.Partner;
import com.muevaelvolante.racenet.model.team.Team_;
import com.muevaelvolante.racenet.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PartnerDetailActivity extends AppCompatActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.pager)
    ViewPager pager;
    private AdapterNewsPager mAdapterNewsPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partner_detail);
        ButterKnife.bind(this);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_back);
        getSupportActionBar().setHomeButtonEnabled(true);
        initViewPager(getIntent().getExtras().getInt("pos"));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()== android.R.id.home)
            finish();
        return true;
    }

    /**
     * Método para la inicialización del ViewPager.
     *
     * @param position
     */
    private void initViewPager(int position) {

        this.mAdapterNewsPager = new AdapterNewsPager(getSupportFragmentManager(), Aplication.partnerList);
        pager.setAdapter(mAdapterNewsPager);
        pager.setCurrentItem(position);

    }

    public class AdapterNewsPager extends FragmentPagerAdapter {

        /*
         * Objeto usuario.
         */
        List<Object> mFeeds;


        /**
         * Constructor por defecto.
         *
         * @param fm
         * @param feeds
         */
        public AdapterNewsPager(android.support.v4.app.FragmentManager fm, List feeds) {

            super(fm);
            this.mFeeds = feeds;

        }


        @Override
        public android.support.v4.app.Fragment getItem(int pos) {

			/*
			 * Todas las pantallas son idénticas, siendo asociada cada una con un objeto Feed.
			 */
            android.support.v4.app.Fragment fragment = PartnerDetailFragment.newInstance((Partner) this.mFeeds.get(pos),"");

            return fragment;

        }


        @Override
        public int getCount() {

            return this.mFeeds.size();

        }
    }
}
