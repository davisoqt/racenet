package com.muevaelvolante.racenet.push;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.mobileconnectors.cognito.Dataset;
import com.amazonaws.mobileconnectors.cognito.Record;
import com.amazonaws.mobileconnectors.cognito.SyncConflict;
import com.amazonaws.mobileconnectors.cognito.exceptions.DataStorageException;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.amazonaws.services.sns.model.GetEndpointAttributesRequest;
import com.amazonaws.services.sns.model.GetEndpointAttributesResult;
import com.amazonaws.services.sns.model.InvalidParameterException;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.NotFoundException;
import com.amazonaws.services.sns.model.SetEndpointAttributesRequest;
import com.amazonaws.services.sns.model.SubscribeResult;
import com.google.android.gms.nearby.messages.internal.SubscribeRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AmazonSNSClientWrapper {

	private final AmazonSNS snsClient;
	private final SharedPreferences pf;

	private String plattaformARN="arn:aws:sns:us-east-1:285406697501:app/GCM/racenetapp_MOBILEHUB_1215091469";

	private String testArn="arn:aws:sns:us-east-1:285406697501:racenetapp_racenettest_MOBILEHUB_1215091469";

	private String endPointARN;
	private boolean updateNeeded;
	private boolean createNeeded;
	Context context;

	public AmazonSNSClientWrapper(AmazonSNS client, Context context) {
		snsClient = new AmazonSNSClient(new BasicAWSCredentials( "AKIAIBEAB2ZGCHDMRVIQ", "mLzlIuLqlAHACz2UtZmh2ma5zvzHPNeKBca5wcDw" ),
				new ClientConfiguration().withMaxConnections(100)
						.withConnectionTimeout(120 * 1000)
						.withSocketTimeout(10000)
						.withMaxErrorRetry(15));
		snsClient.setRegion(Region.getRegion(Regions.US_EAST_1));
		pf= context.getSharedPreferences("GCM",Context.MODE_PRIVATE);
		this.context=context;
	}

	private CreatePlatformEndpointResult createPlatformEndpoint(String customData, String platformToken) {
		CreatePlatformEndpointRequest platformEndpointRequest = new CreatePlatformEndpointRequest();
		platformEndpointRequest.setCustomUserData(Build.MANUFACTURER+" "+ Build.MODEL);
		String token = platformToken;
		platformEndpointRequest.setToken(token);
		platformEndpointRequest.setPlatformApplicationArn(plattaformARN);
		return snsClient.createPlatformEndpoint(platformEndpointRequest);
	}

	public void registerChanels(String platformToken, String [] chanels) {
		try{
		endPointARN = pf.getString("arn","");
		Log.d("ARN",endPointARN);
		//si no tengo un arn, lo registro
		if (endPointARN == null || TextUtils.isEmpty(endPointARN)) {
			createARN(platformToken);
		}
		try {
			GetEndpointAttributesRequest geaReq = new GetEndpointAttributesRequest()
							.withEndpointArn(endPointARN);
			GetEndpointAttributesResult geaRes = snsClient.getEndpointAttributes(geaReq);

			updateNeeded = !geaRes.getAttributes().get("Token").equals(platformToken)
					|| !geaRes.getAttributes().get("Enabled").equalsIgnoreCase("true");

		} catch (NotFoundException nfe) {
			// We had a stored ARN, but the platform endpoint associated with it
			// disappeared. Recreate it.
			createNeeded = true;
		}
		if (createNeeded) {
			createARN(platformToken);
		}

		System.out.println("updateNeeded = " + updateNeeded);

		if (updateNeeded) {
			// The platform endpoint is out of sync with the current data;
			// update the token and enable it.
			System.out.println("Updating platform endpoint " + endPointARN);
			Map attribs = new HashMap();
			attribs.put("Token", platformToken);
			attribs.put("Enabled", "true");
			SetEndpointAttributesRequest saeReq =
					new SetEndpointAttributesRequest()
							.withEndpointArn(endPointARN)
							.withAttributes(attribs);
			snsClient.setEndpointAttributes(saeReq);
			pf.edit().putString("token",platformToken).apply();
		}

		SharedPreferences chanelPf= context.getSharedPreferences("chanels",Context.MODE_PRIVATE);
		SharedPreferences arnPf= context.getSharedPreferences("arn",Context.MODE_PRIVATE);//(chanel,arn)
		for (String chanel :
				chanels) {
			com.amazonaws.services.sns.model.SubscribeRequest subscribeRequest = new com.amazonaws.services.sns.model.SubscribeRequest
					(chanel, "application", endPointARN);
			SubscribeResult subscribeResult = snsClient.subscribe(subscribeRequest);
			String subscriptionArn = subscribeResult.getSubscriptionArn();
			if(subscriptionArn != null && !TextUtils.isEmpty(subscriptionArn)){
				arnPf.edit().putString(chanel,subscriptionArn).commit();
                chanelPf.edit().putInt(chanel,1).commit();
				Log.d(chanel,subscriptionArn);
			}
		}
		//here handle chanel subscription

		//start whith news
		//String newsAlert= dataSet.get(Constants.NOTIFICATION_NEWS);
		String newsSubsCriptionArn= pf.getString("test","");
//		if(newsAlert.equals("0") && !TextUtils.isEmpty(newsSubsCriptionArn)){
//			try {
//				Log.d("Quitando"," subscripcion de news.....");
//				snsClient.unsubscribe(newsSubsCriptionArn);
//				pf.edit().putString(Constants.NOTIFICATION_NEWS_ARN,"").apply();
//				Log.d(""," subscripcion eliminada.....");
//			}catch (Exception ex){
//				Log.d("error","esta pinga no pincha");
//				ex.printStackTrace();
//			}
//
//		}
//		else if(newsAlert.equals("1")) {

//		}
//			//now with racing
//			String racingAlert= dataSet.get(Constants.NOTIFICATION_RACING_ALERT);
//			String racingSubsCriptionArn= pf.getString(Constants.NOTIFICATION_RACING_ARN,"");
//			if(racingAlert.equals("0") && !TextUtils.isEmpty(racingSubsCriptionArn)) {
//				try {
//					snsClient.unsubscribe(racingSubsCriptionArn);
//					pf.edit().putString(Constants.NOTIFICATION_RACING_ARN, "").apply();
//				} catch (Exception ex) {
//					ex.printStackTrace();
//				}
//			}
//			else if(racingAlert.equals("1")) {
//				com.amazonaws.services.sns.model.SubscribeRequest subscribeRequest = new com.amazonaws.services.sns.model.SubscribeRequest
//						(racingArn, "application", endPointARN);
//				SubscribeResult subscribeResult = snsClient.subscribe(subscribeRequest);
//				String subscriptionArn = subscribeResult.getSubscriptionArn();
//				if(subscriptionArn != null && !TextUtils.isEmpty(subscriptionArn)){
//					pf.edit().putString(Constants.NOTIFICATION_RACING_ARN,subscriptionArn).apply();
//					Log.d("Racing chanel",subscriptionArn);
//				}
//			}
//			//guardamos las preferencia en cognito
//			dataSet.synchronize(new Dataset.SyncCallback() {
//				@Override
//				public void onSuccess(Dataset dataset, List<Record> list) {
//
//				}
//
//				@Override
//				public boolean onConflict(Dataset dataset, List<SyncConflict> list) {
//					return false;
//				}
//
//				@Override
//				public boolean onDatasetDeleted(Dataset dataset, String s) {
//					return false;
//				}
//
//				@Override
//				public boolean onDatasetsMerged(Dataset dataset, List<String> list) {
//					return false;
//				}
//
//				@Override
//				public void onFailure(DataStorageException e) {
//
//				}
//			});
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}

	void createARN(String platformToken){
		CreatePlatformEndpointResult platformEndpointResult = createPlatformEndpoint(
				Build.MANUFACTURER+" "+Build.MODEL,
				platformToken);
		try {
			endPointARN = platformEndpointResult.getEndpointArn();
			Log.d("ARN",endPointARN);
		}catch (InvalidParameterException ipe){
			String message = ipe.getErrorMessage();
			System.out.println("Exception message: " + message);
			Pattern p = Pattern
					.compile(".*Endpoint (arn:aws:sns[^ ]+) already exists " +
							"with the same token.*");
			Matcher m = p.matcher(message);
			if (m.matches()) {
				// The platform endpoint already exists for this token, but with
				// additional custom data that
				// createEndpoint doesn't want to overwrite. Just use the
				// existing platform endpoint.
				endPointARN = m.group(1);
			} else {
				// Rethrow the exception, the input is actually bad.
				throw ipe;
			}
		}
		pf.edit().putString("token",platformToken).apply();
		pf.edit().putString("arn",endPointARN).apply();
		System.out.println(platformEndpointResult);
	}
}
