package com.muevaelvolante.racenet.push;

import android.app.ActivityManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.SplashActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Set;


/**
 * Created by Leo on 09/05/2016.
 */
@SuppressWarnings("WrongConstant")
public class NotifyIntentService extends IntentService {

    public NotifyIntentService() {
        super("gjgjfghf");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
       // try {
        Bundle bundle = intent.getExtras();
        String mfs= bundle.getString("message");
        try {
            JSONObject object= new JSONObject(mfs);
            String title= object.getString("title");
            String message= object.getString("msg");
            String action= object.getString("action");
            String target= object.getString("target");
            Log.d("json", object.getString("msg"));
            Intent pending= new Intent(getApplicationContext(), SplashActivity.class);
            pending.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle data= new Bundle();
            data.putString("action",action);
            data.putString("target",target);
            pending.putExtras(data);
            postNotification(title,message,pending,null);
            Intent sendToHome= new Intent("PUSH_ADDED");
            sendToHome.putExtras(bundle);
            LocalBroadcastManager.getInstance(this).sendBroadcast(sendToHome);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void postNotification(String title, String msg, Intent intentAction, NotifyIntentService notifyIntentService) {
        final NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        final PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intentAction, Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL|PendingIntent.FLAG_UPDATE_CURRENT);
        Uri sound= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        long[] pattern = new long[] { 500, 500, 1000, 1000, 500, 500 };
        final Notification notification = new NotificationCompat.Builder(this).setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(msg)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setSound(sound)
                .setVibrate(pattern)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .getNotification();
        mNotificationManager.notify(23, notification);
        PowerManager pm = (PowerManager)this.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
        if(isScreenOn==false)
        {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MyLock");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MyCpuLock");
            wl_cpu.acquire(3000);
        }
    }
    public void testNoty(){
        final NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        Uri sound= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        long[] pattern = new long[] { 500, 500, 1000, 1000, 500, 500 };
        final Notification notification = new NotificationCompat.Builder(this).setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Prueba")
                .setContentText("Prueba")
                .setAutoCancel(true)
                .setSound(sound)
                .setVibrate(pattern)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .getNotification();
        mNotificationManager.notify(23, notification);
        PowerManager pm = (PowerManager)this.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
        if(isScreenOn==false)
        {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MyLock");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MyCpuLock");
            wl_cpu.acquire(3000);
        }
    }
}
