package com.muevaelvolante.racenet.commons;

import android.os.Environment;

public class Constants {

	public static final boolean DEVELOPER_DEBUG = true;

	/*
	* CONSTANTES DE PREFERENCIAS
	* */

	public static String appRootDir=Environment.getExternalStorageDirectory()+"/RaceNET";
	public static String jsonCacheRootDir= Environment.getExternalStorageDirectory()+"/RaceNET/cache";


	
	/*
	*Service updater json
	* */
	public static final String JSON_UPDATE_RUNNING = "is_running";
	public static final String JSON_UPDATE_PREFERENCE = "json";
	public static final String JSON_UPDATE_NEEDED = "update_needed";
	public static final String JSON_UPDATE_HEADER = "update_header";


	/*
	 * Clave de Google Developer.
	 */
	public static final String DEVELOPER_KEY = "AIzaSyD3VMFqpu0w_YvgSq0OdlGDbNNNH_4BkIs";

	public static final String Direct_Market_app = "market://details?id=";
	public static final String URL_Market_app = "http://play.google.com/store/apps/details?id=";
	
}