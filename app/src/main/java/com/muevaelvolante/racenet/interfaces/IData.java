package com.muevaelvolante.racenet.interfaces;

import com.muevaelvolante.racenet.model.web.ApiWeb;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by Leo on 07/08/2016.
 */
public interface IData {
    @GET
    public Call<ResponseBody> getData(@Url String url);
}
