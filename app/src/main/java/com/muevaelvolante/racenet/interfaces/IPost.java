package com.muevaelvolante.racenet.interfaces;

import com.muevaelvolante.racenet.model.post.Post;
import com.muevaelvolante.racenet.model.search.Search;
import com.muevaelvolante.racenet.model.setting.Setting;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by Leo on 05/08/2016.
 */
public interface IPost {
    @GET
    public Call<Post> getPosts(@Url String url);
    @GET
    public Call<Search> search(@Url String url);
}
