package com.muevaelvolante.racenet.interfaces;

import com.muevaelvolante.racenet.model.post.Post;
import com.muevaelvolante.racenet.model.web.ApiWeb;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by Leo on 05/08/2016.
 */
public interface ILoginWeb {
    @GET
    public Call<ApiWeb> login(@Url String url,@QueryMap Map<String,String> mapData);
}
