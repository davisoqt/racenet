package com.muevaelvolante.racenet.interfaces;

import com.muevaelvolante.racenet.model.setting.Setting;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by Leo on 03/08/2016.
 */
public interface ISettings {
    @GET
    public Call<Setting> getData(@Url String url);

    @GET
    @Streaming
    Call<ResponseBody> downloadFilePdf(@Url String fileUrl);
}
